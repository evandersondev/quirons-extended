import 'package:flutter/material.dart';
import 'package:quirons_user/app/inject/inject.dart';
import 'quirons_user_app.dart';

void main() {
  Inject.init();

  runApp(const QuironsUserApp());
}
