import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'app/views/index_page.dart';
import 'app/views/login/login_view.dart';

import 'app/services/auth_service.dart';
import 'app/services/error_service.dart';
import 'app/services/gql_service.dart';
import 'app/utils/adaptive_dialog.dart';
import 'app/utils/graphql_client_provider.dart';
import 'app/utils/is_debug.dart';
import 'generated_env.dart';

class QuironsUserAppController {
  void handleSessionDrop(ServerRejection error) async {
    final client = Get.find<GQLService>();
    final auth = Get.find<AuthService>();

    client.resetCache();
    await auth.clearToken();

    Get.offAll(() => LoginView());
    unawaited(Get.dialog(AdaptiveDialog.alert(
      title: const Text('Conta desconectada'),
      content: Text(error.message!),
    )));
  }

  Future registerServices() async {
    final packageInfo = await PackageInfo.fromPlatform();
    final dio = Get.put(Dio())
      ..interceptors.add(
        InterceptorsWrapper(
          onRequest: (options, handler) async {
            final auth = AuthService();
            final token = await auth.getToken();

            options.headers['user-agent'] =
                'QuironsMobile/${packageInfo.version}:${packageInfo.buildNumber}';

            options.headers['Authorization'] = token;

            return handler.next(options);
          },
        ),
      );

    if (isDebug()) {
      dio.interceptors.add(LogInterceptor());
    }

    final client = Get.put<GraphQLClient>(
      buildClient(
        dio: dio,
        uri: '$quironsUrl/graphql',
        onGraphQLError: (request, forward, response) {
          final service = ErrorService();
          final error = service.parseList(response.errors!).first;

          if (error.code == SupportedErrorCodes.invalidSession ||
              error.code == SupportedErrorCodes.deletedSession) {
            handleSessionDrop(error);
          }

          return null;
        },
      ),
    );

    Get.put(GQLService(client));
  }

  void decideInitialRoute() async {
    final auth = AuthService();

    final hasToken = await auth.hasToken();

    if (hasToken) {
      Get.offAll(() => IndexPage());
    } else {
      Get.offAll(() => LoginView());
    }
  }
}
