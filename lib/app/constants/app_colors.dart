import 'package:flutter/material.dart';

const primaryColor = MaterialColor(0xFF152849, {
  50: Color(0xFF152849),
  100: Color(0xFF152849),
  200: Color(0xFF152849),
  300: Color(0xFF152849),
  400: Color(0xFF152849),
  500: Color(0xFF152849),
  600: Color(0xFF152849),
  700: Color(0xFF152849),
  800: Color(0xFF152849),
  900: Color(0xFF152849),
});

const successColor = Color(0xFF0E9043);
const successLightColor = Color(0xFFE9F4DD);
const successLowColor = Color(0xFF8BC34A);
const warningColor = Color(0xFFFFA000);
const warningLightColor = Color(0xFFFFF7D5);
const warningLowColor = Color(0xFFE7BF11);
const dangerColor = Color(0xFFD84315);
const dangerLightColor = Color(0xFFFFCDBE);
const infoColor = Color(0xFF2F80ED);
const infoLightColor = Color(0xFFDAF2FD);
const infoLowColor = Color(0xFF56CCF2);
const browColor = Color(0xFF83471E);
const orangeColor = Color(0xFFFF9900);
const scaffoldColor = Color(0xFFF6F7FF);
const textColor = Color(0xFF212121);
const textLightColor = Color(0xFF757575);
