import 'package:dartz/dartz.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../graphql/gen/api.graphql.dart';

abstract class IRiskRepository {
  Future<Either<OperationException, List<EmployeeRisks$Query$EmployeeRisks>>>
      list(BaseInput input);
}
