import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../graphql/gen/api.graphql.dart';

abstract class ILoginRepository {
  Future<Either<OperationException, EmployeeLogin$Query>> login(
    BuildContext context, {
    required BaseInput input,
  });
}
