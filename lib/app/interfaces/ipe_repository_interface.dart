import 'package:dartz/dartz.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../graphql/gen/api.graphql.dart';

abstract class IIpeRepository {
  Future<Either<OperationException, List<EmployeeIpes$Query$EmployeeIpes>>>
      list(BaseInput input);
}
