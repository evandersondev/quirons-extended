import 'package:dartz/dartz.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../graphql/gen/api.graphql.dart';

abstract class IProfileRepository {
  Future<Either<OperationException, EmployeeInfo$Query$EmployeeInfo>> get(
      BaseInput input);
}
