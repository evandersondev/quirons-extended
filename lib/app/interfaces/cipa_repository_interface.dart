import 'package:dartz/dartz.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../graphql/gen/api.graphql.dart';

abstract class ICipaRepository {
  Future<
      Either<OperationException,
          List<EmployeeMandates$Query$EmployeeMandates>>> mandates(
    EmployeeMandateInput input,
  );

  Future<
      Either<OperationException,
          EmployeeCandidate$Mutation$EmployeeCandidate>> candidate(
      EmployeeCandidateInput input);

  Future<Either<OperationException, EmployeeVote$Mutation$EmployeeVote>> vote(
      EmployeeVoteInput input);

  Future<
      Either<OperationException,
          EmployeeHasCandidate$Query$EmployeeHasCandidate>> hasCandidate(
      EmployeeCandidateInput input);

  Future<Either<OperationException, EmployeeHasVote$Query$EmployeeHasVote?>>
      hasVote(EmployeeHasVoteInput input);

  Future<Either<OperationException, List<EmployeeIpes$Query$EmployeeIpes>>>
      ipes(BaseInput input);

  Future<
          Either<OperationException,
              List<EmployeeConsultations$Query$EmployeeConsultations>>>
      consultation(BaseInput input);
}
