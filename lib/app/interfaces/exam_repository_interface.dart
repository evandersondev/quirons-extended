import 'package:dartz/dartz.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../graphql/gen/api.graphql.dart';

abstract class IExamRepository {
  Future<
      Either<OperationException,
          List<EmployeeConsultations$Query$EmployeeConsultations>>> list(
      BaseInput input);
}
