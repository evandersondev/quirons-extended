import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../graphql/gen/api.graphql.dart';

abstract class IRegisterRepository {
  Future<Either<OperationException, GeneratePassword$Mutation>> register(
    BuildContext context, {
    required GeneratePasswordInput input,
  });
}
