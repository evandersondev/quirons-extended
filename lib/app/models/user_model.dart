import 'dart:convert';

class UserModel {
  final String name;
  final String individualRegistration;
  final String registration;
  final String organizationId;
  final String password;
  final DateTime birthday;
  UserModel({
    required this.name,
    required this.individualRegistration,
    required this.registration,
    required this.organizationId,
    required this.password,
    required this.birthday,
  });

  UserModel copyWith({
    String? name,
    String? individualRegistration,
    String? registration,
    String? organizationId,
    String? password,
    DateTime? birthday,
  }) {
    return UserModel(
      name: name ?? this.name,
      individualRegistration:
          individualRegistration ?? this.individualRegistration,
      registration: registration ?? this.registration,
      organizationId: organizationId ?? this.organizationId,
      password: password ?? this.password,
      birthday: birthday ?? this.birthday,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'individualRegistration': individualRegistration,
      'registration': registration,
      'organizationId': organizationId,
      'password': password,
      'birthday': birthday.millisecondsSinceEpoch,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      name: map['name'] ?? '',
      individualRegistration: map['individualRegistration'] ?? '',
      registration: map['registration'] ?? '',
      organizationId: map['organizationId'] ?? '',
      password: map['password'] ?? '',
      birthday: DateTime.fromMillisecondsSinceEpoch(map['birthday']),
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UserModel(name: $name, individualRegistration: $individualRegistration, registration: $registration, organizationId: $organizationId, password: $password, birthday: $birthday)';
  }
}
