import 'dart:convert';

class OrganizationModel {
  final String? id;
  final String? name;
  OrganizationModel({
    this.id,
    this.name,
  });

  OrganizationModel copyWith({
    String? id,
    String? name,
  }) {
    return OrganizationModel(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory OrganizationModel.fromMap(Map<String, dynamic> map) {
    return OrganizationModel(
      id: map['id'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory OrganizationModel.fromJson(String source) =>
      OrganizationModel.fromMap(json.decode(source));

  @override
  String toString() => 'OrganizationModel(id: $id, name: $name)';
}
