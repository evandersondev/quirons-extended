import 'package:flutter/material.dart';
import 'package:quirons_user/app/interfaces/ipe_repository_interface.dart';
import 'package:quirons_user/app/models/user_model.dart';
import 'package:quirons_user/app/services/auth_service.dart';
import 'package:quirons_user/app/widgets/error_handlers.dart';
import '../graphql/gen/api.graphql.dart';

class IpeController {
  final IIpeRepository repository;

  IpeController(this.repository) {
    getCurrentUser();
  }

  Future<void> init() async {
    await list();
  }

  final auth = AuthService();
  final loading = ValueNotifier<bool>(false);
  final currentUser = ValueNotifier<UserModel>(UserModel(
    birthday: DateTime.now(),
    individualRegistration: '',
    name: '',
    organizationId: '',
    password: '',
    registration: '',
  ));
  final ipesList = ValueNotifier<List<EmployeeIpes$Query$EmployeeIpes>>([]);

  Future<void> getCurrentUser() async {
    final token = await auth.getToken();

    if (token != null) {
      final user = UserModel.fromJson(token);

      currentUser.value = user;
      init();
    }
  }

  Future<void> list() async {
    loading.value = true;

    final input = BaseInput(
      organizationId: currentUser.value.organizationId,
      individualRegistration: currentUser.value.individualRegistration,
      password: currentUser.value.password,
    );

    final response = await repository.list(input);
    response.fold(
      (exception) {
        handleOrIgnoreError(exception);
      },
      (success) {
        ipesList.value = success;
        print(success.length);
      },
    );

    loading.value = false;
  }
}
