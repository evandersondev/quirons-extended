import 'package:flutter/material.dart';
import 'package:quirons_user/app/interfaces/risk_repository_interface.dart';

import '../graphql/gen/api.graphql.dart';
import '../models/user_model.dart';
import '../services/auth_service.dart';
import '../widgets/error_handlers.dart';

class RiskController {
  IRiskRepository _repository;

  RiskController(
    this._repository,
  ) {
    getCurrentUser();
  }

  Future<void> getCurrentUser() async {
    final token = await auth.getToken();

    if (token != null) {
      final user = UserModel.fromJson(token);

      currentUser.value = user;
      await getRisks();
    }
  }

  final auth = AuthService();
  final loading = ValueNotifier<bool>(false);
  final riskList = ValueNotifier<List<EmployeeRisks$Query$EmployeeRisks>>([]);
  final currentUser = ValueNotifier<UserModel>(UserModel(
    birthday: DateTime.now(),
    individualRegistration: '',
    name: '',
    organizationId: '',
    password: '',
    registration: '',
  ));

  Future<void> getRisks() async {
    loading.value = true;

    final response = await _repository.list(BaseInput(
      organizationId: currentUser.value.organizationId,
      individualRegistration: currentUser.value.individualRegistration,
      password: currentUser.value.password,
    ));

    await response.fold((exception) {
      handleOrIgnoreError(exception);
    }, (risks) {
      riskList.value = risks;
    });

    loading.value = false;
  }
}
