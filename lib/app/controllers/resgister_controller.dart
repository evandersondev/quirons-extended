import 'package:quirons_user/app/constants/app_colors.dart';
import 'package:quirons_user/app/interfaces/register_repository_interface.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:quirons_user/app/utils/get_organization_from_qrcode.dart';
import 'package:quirons_user/app/widgets/error_handlers.dart';
import '../models/organization_model.dart';
import '../graphql/gen/api.graphql.dart';

class RegisterController {
  IRegisterRepository repository;

  RegisterController(this.repository);

  final formKey = GlobalKey<FormState>();
  final organization = ValueNotifier<OrganizationModel?>(null);
  final organizationController = TextEditingController();
  final registrationController = TextEditingController();
  final cpfController = TextEditingController();
  final passwordController = TextEditingController();
  final birthDateController = TextEditingController();
  final operationType =
      ValueNotifier<EmployeeOperationType>(EmployeeOperationType.create);
  final showPassword = ValueNotifier<bool>(false);
  final loading = ValueNotifier<bool>(false);

  void getOrganization() async {
    final response = await getOrganizationFromQRCode();

    if (response == null) return;

    organization.value = response;
    organizationController.text = response.name!;
  }

  void updateBirthDate(DateTime date) {
    birthDateController.text = DateFormat('dd/MM/yyyy').format(date);
  }

  void shouldShowPassword() {
    showPassword.value = !showPassword.value;
  }

  void updateOperationType(EmployeeOperationType type) {
    operationType.value = type;
  }

  Future<void> handleRegisterSubmit(BuildContext context) async {
    if (loading.value || !formKey.currentState!.validate()) {
      return;
    }

    loading.value = true;

    final input = GeneratePasswordInput(
      birthday: DateTime.parse(
          birthDateController.text.split('/').reversed.join('-')),
      organizationId: organization.value!.id!,
      individualRegistration: cpfController.text,
      password: passwordController.text,
      registration: registrationController.text,
      operation: operationType.value,
    );

    final response = await repository.register(context, input: input);

    response.fold((exception) {
      handleOrIgnoreError(exception);
    }, (success) {
      Navigator.pop(context);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Cadastro Atualizado com sucesso!'),
        backgroundColor: successColor,
        behavior: SnackBarBehavior.floating,
        margin: EdgeInsets.all(12),
      ));
    });

    loading.value = false;
  }
}
