import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quirons_user/app/interfaces/exam_repository_interface.dart';

import '../graphql/gen/api.graphql.dart';
import '../models/user_model.dart';
import '../services/auth_service.dart';
import '../services/gql_service.dart';
import '../widgets/error_handlers.dart';

class ExamController {
  IExamRepository _repository;
  ExamController(
    this._repository,
  ) {
    getCurrentUser();
  }

  Future<void> getCurrentUser() async {
    final token = await auth.getToken();

    if (token != null) {
      final user = UserModel.fromJson(token);

      currentUser.value = user;
      init();
    }
  }

  final client = Get.find<GQLService>();
  final auth = AuthService();
  final loading = ValueNotifier<bool>(false);
  final examsList =
      ValueNotifier<List<EmployeeConsultations$Query$EmployeeConsultations>>(
          []);
  final currentUser = ValueNotifier<UserModel>(UserModel(
    birthday: DateTime.now(),
    individualRegistration: '',
    name: '',
    organizationId: '',
    password: '',
    registration: '',
  ));

  Future<void> init() async {
    await getExams();
  }

  Future<void> getExams() async {
    loading.value = true;

    final response = await _repository.list(BaseInput(
      organizationId: currentUser.value.organizationId,
      individualRegistration: currentUser.value.individualRegistration,
      password: currentUser.value.password,
    ));

    await response.fold((exception) {
      handleOrIgnoreError(exception);
    }, (examList) {
      if (examList.isNotEmpty) {
        var exams = examList.where((e) => e.startDate == null).toList();
        exams.sort((a, b) => a.estimatedDate.compareTo(b.estimatedDate));
        examsList.value = examList;
      } else {
        examsList.value = [];
      }
    });

    loading.value = false;
  }
}
