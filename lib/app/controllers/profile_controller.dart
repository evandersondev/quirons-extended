import 'package:flutter/material.dart';
import 'package:quirons_user/app/interfaces/profile_repository_interface.dart';

import '../graphql/gen/api.graphql.dart';
import '../models/user_model.dart';
import '../services/auth_service.dart';
import '../widgets/error_handlers.dart';

class ProfileController {
  IProfileRepository _repository;

  ProfileController(
    this._repository,
  ) {
    getCurrentUser();
  }

  Future<void> getCurrentUser() async {
    final token = await auth.getToken();

    if (token != null) {
      final user = UserModel.fromJson(token);

      currentUser.value = user;
      await getUserInfos();
    }
  }

  final auth = AuthService();
  final loading = ValueNotifier<bool>(false);
  final userInfo = ValueNotifier<EmployeeInfo$Query$EmployeeInfo?>(null);
  final currentUser = ValueNotifier<UserModel>(UserModel(
    birthday: DateTime.now(),
    individualRegistration: '',
    name: '',
    organizationId: '',
    password: '',
    registration: '',
  ));

  Future<void> getUserInfos() async {
    loading.value = true;

    final response = await _repository.get(BaseInput(
      organizationId: currentUser.value.organizationId,
      individualRegistration: currentUser.value.individualRegistration,
      password: currentUser.value.password,
    ));

    await response.fold((exception) {
      handleOrIgnoreError(exception);
    }, (infos) {
      userInfo.value = infos;
    });

    loading.value = false;
  }
}
