import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quirons_user/app/constants/app_colors.dart';
import 'package:quirons_user/app/interfaces/cipa_repository_interface.dart';
import '../graphql/gen/api.graphql.dart';
import '../models/user_model.dart';
import '../services/auth_service.dart';
import '../services/gql_service.dart';
import '../widgets/error_handlers.dart';

class CipaController {
  ICipaRepository _repository;

  CipaController(
    this._repository,
  ) {
    getCurrentUser();
  }

  final client = Get.find<GQLService>();
  final auth = AuthService();
  final employeeMandateList =
      ValueNotifier<List<EmployeeMandates$Query$EmployeeMandates>>([]);
  final currentUser = ValueNotifier<UserModel>(UserModel(
    birthday: DateTime.now(),
    individualRegistration: '',
    name: '',
    organizationId: '',
    password: '',
    registration: '',
  ));
  final listMandatesToCandidate =
      ValueNotifier<List<EmployeeMandates$Query$EmployeeMandates?>>([]);
  final listMandatesToVote =
      ValueNotifier<List<EmployeeMandates$Query$EmployeeMandates?>>([]);
  final loading = ValueNotifier<bool>(false);
  final hasCandidate = ValueNotifier<bool>(false);
  final hasVoted = ValueNotifier<bool>(false);
  final ipeNextChange = ValueNotifier<EmployeeIpes$Query$EmployeeIpes?>(null);
  final examEstimate =
      ValueNotifier<EmployeeConsultations$Query$EmployeeConsultations?>(null);

  Future<void> getCurrentUser() async {
    final token = await auth.getToken();

    if (token != null) {
      final user = UserModel.fromJson(token);

      currentUser.value = user;
      init();
    }
  }

  Future<void> init() async {
    await getIpes();
    await getExams();
    listMandatesToCandidate.value =
        await getMandates(MandateEventType.candidate);
    listMandatesToVote.value = await getMandates(MandateEventType.vote);
    hasCandidate.value = await checkIfAlreadyIsCandidate();
    hasVoted.value = await checkIfAlreadyVoted();
  }

  Future<bool> checkIfAlreadyIsCandidate() async {
    loading.value = true;
    bool hasCandidate = false;

    final response = await _repository.hasCandidate(
      EmployeeCandidateInput(
        organizationId: currentUser.value.organizationId,
        individualRegistration: currentUser.value.individualRegistration,
        password: currentUser.value.password,
        mandateId: listMandatesToCandidate.value.isNotEmpty
            ? listMandatesToCandidate.value.first!.mandateId
            : '',
      ),
    );

    await response.fold((exception) {}, (candidate) {
      hasCandidate = true;
    });

    loading.value = false;
    return hasCandidate;
  }

  Future<bool> checkIfAlreadyVoted() async {
    loading.value = true;
    bool hasVoted = false;

    final response = await _repository.hasVote(
      EmployeeHasVoteInput(
        individualRegistration: currentUser.value.individualRegistration,
        mandateId: listMandatesToVote.value.isNotEmpty
            ? listMandatesToVote.value.first!.mandateId
            : '',
        organizationId: currentUser.value.organizationId,
        password: currentUser.value.password,
      ),
    );

    await response.fold((exception) {}, (vote) {
      hasVoted = true;
    });

    loading.value = false;
    return hasVoted;
  }

  Future sendMyVote(
    BuildContext context, {
    required String mandateId,
    bool? nullVote,
    bool? whiteVote,
    EmployeeCandidateFragmentMixin$Person? person,
  }) async {
    loading.value = true;

    final response = await _repository.vote(EmployeeVoteInput(
      individualRegistration: currentUser.value.individualRegistration,
      organizationId: currentUser.value.organizationId,
      password: currentUser.value.password,
      mandateId: mandateId,
      candidateId: person?.id == '' ? null : person?.id,
      nullVote: nullVote,
      whiteVote: whiteVote,
    ));

    response.fold((exception) {
      handleOrIgnoreError(exception);
    }, (mandateList) async {
      Navigator.pop(context);
      Navigator.pop(context);

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Row(
          children: [
            Icon(Icons.check_circle, color: Colors.white),
            SizedBox(width: 4),
            Text('Votação realizada'),
          ],
        ),
        backgroundColor: successColor,
        behavior: SnackBarBehavior.floating,
        margin: EdgeInsets.all(12),
      ));

      await init();
    });

    loading.value = false;
  }

  Future<bool> sendMyCanditature() async {
    loading.value = true;
    bool candidate = false;

    final response = await _repository.candidate(EmployeeCandidateInput(
      individualRegistration: currentUser.value.individualRegistration,
      organizationId: currentUser.value.organizationId,
      password: currentUser.value.password,
      mandateId: listMandatesToCandidate.value.first?.mandateId ?? '',
    ));

    await response.fold((exception) {
      candidate = false;
      handleOrIgnoreError(exception);
    }, (candidature) {
      candidate = true;
    });

    loading.value = false;

    return candidate;
  }

  Future<List<EmployeeMandates$Query$EmployeeMandates>> getMandates(
      MandateEventType type) async {
    loading.value = true;

    final response = await _repository.mandates(
      EmployeeMandateInput(
        organizationId: currentUser.value.organizationId,
        individualRegistration: currentUser.value.individualRegistration,
        password: currentUser.value.password,
        type: type,
      ),
    );

    List<EmployeeMandates$Query$EmployeeMandates> list = [];

    await response.fold((exception) {
      handleOrIgnoreError(exception);
    }, (mandateList) {
      list = mandateList;
    });

    loading.value = false;

    return list;
  }

  Future<void> getIpes() async {
    loading.value = true;

    final response = await _repository.ipes(BaseInput(
      organizationId: currentUser.value.organizationId,
      individualRegistration: currentUser.value.individualRegistration,
      password: currentUser.value.password,
    ));

    await response.fold((exception) {
      handleOrIgnoreError(exception);
    }, (ipesList) {
      if (ipesList.isNotEmpty) {
        ipesList.sort((a, b) => a.nextChange!.compareTo(b.nextChange!));
        ipeNextChange.value = ipesList.first;
      } else {
        ipeNextChange.value = null;
      }
    });

    loading.value = false;
  }

  Future<void> getExams() async {
    loading.value = true;

    final response = await _repository.consultation(BaseInput(
      organizationId: currentUser.value.organizationId,
      individualRegistration: currentUser.value.individualRegistration,
      password: currentUser.value.password,
    ));

    await response.fold((exception) {
      handleOrIgnoreError(exception);
    }, (examList) {
      if (examList.isNotEmpty) {
        var exams = examList.where((e) => e.startDate == null).toList();
        exams.sort((a, b) => a.estimatedDate.compareTo(b.estimatedDate));
        examEstimate.value = examList.first;
      } else {
        examEstimate.value = null;
      }
    });

    loading.value = false;
  }
}
