import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:get/route_manager.dart';
import 'package:quirons_user/app/interfaces/login_repository_interface.dart';
import 'package:quirons_user/app/models/user_model.dart';
import 'package:quirons_user/app/services/auth_service.dart';
import 'package:quirons_user/app/utils/get_organization_from_qrcode.dart';
import 'package:quirons_user/app/views/index_page.dart';
import 'package:quirons_user/app/widgets/error_handlers.dart';
import '../models/organization_model.dart';
import '../graphql/gen/api.graphql.dart';

class LoginController {
  ILoginRepository repository;

  LoginController(this.repository) {
    keyboardVisibilityController.onChange.listen((visible) {
      if (visible) {
        isGrowing.value = true;
      } else {
        isGrowing.value = false;
      }
    });
  }

  final auth = AuthService();
  final organization = ValueNotifier<OrganizationModel?>(null);
  final formKey = GlobalKey<FormState>();
  final keyboardVisibilityController = KeyboardVisibilityController();
  final organizationController = TextEditingController();
  final cpfController = TextEditingController();
  final passwordController = TextEditingController();
  final isGrowing = ValueNotifier<bool>(false);
  final showPassword = ValueNotifier<bool>(false);
  final loading = ValueNotifier<bool>(false);

  void shouldShowPassword() {
    showPassword.value = !showPassword.value;
  }

  void getOrganization() async {
    final response = await getOrganizationFromQRCode();

    if (response == null) return;

    organization.value = response;
    organizationController.text = response.name!;
  }

  Future<void> handleLoginSubmit(BuildContext context) async {
    if (loading.value ||
        organizationController.text.isEmpty ||
        !formKey.currentState!.validate()) {
      return;
    }

    loading.value = true;

    final input = BaseInput(
      organizationId: organization.value!.id!,
      individualRegistration: cpfController.text,
      password: passwordController.text,
    );

    final response = await repository.login(context, input: input);
    response.fold(
      (exception) {
        handleOrIgnoreError(exception);
      },
      (success) {
        final user = UserModel(
          name: success.employeeLogin.name,
          individualRegistration: success.employeeLogin.individualRegistration,
          registration: success.employeeLogin.registration ?? '',
          birthday: success.employeeLogin.birthday,
          password: passwordController.text,
          organizationId: organization.value!.id!,
        );

        auth.saveToken(user.toJson());
        Get.offAll(() => IndexPage());
      },
    );

    loading.value = false;
  }
}
