import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:quirons_user/app/constants/app_colors.dart';
import 'package:quirons_user/app/constants/app_metrics.dart';
import 'package:quirons_user/app/widgets/alert_widget.dart';

class AlertCandidateSuccessWidget {
  static Future show(BuildContext context) async {
    return AlertWidget.show(AlertAnimateEnum.fade,
        context: context,
        title: SvgPicture.asset(
          'lib/app/assets/icons/cipa.svg',
          height: MediaQuery.of(context).size.width / 6,
          color: successLowColor,
        ),
        body: [
          SizedBox(height: spaceSize),
          const Text(
            'PARABÉNS PELA CANDIDATURA!',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: spaceSize),
          const Text(
            'A partir de agora você está concorrendo na CIPA 2022/2023.',
            textAlign: TextAlign.center,
          ),
          const Text(
            'Boa sorte!',
            textAlign: TextAlign.center,
          ),
        ],
        actions: [
          ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: Container(
              color: primaryColor,
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: TextButton(
                child: const Text('FECHAR',
                    style: TextStyle(
                      color: Colors.white,
                    )),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
          ),
        ]);
  }
}
