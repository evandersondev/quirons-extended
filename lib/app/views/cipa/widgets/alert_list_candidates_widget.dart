import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:quirons_user/app/constants/app_colors.dart';
import 'package:quirons_user/app/constants/app_metrics.dart';
import 'package:quirons_user/app/models/user_model.dart';
import 'package:quirons_user/app/widgets/alert_widget.dart';

import '../../../graphql/gen/api.graphql.dart';

class AlertListCandidatesWidget extends StatefulWidget {
  final BuildContext context;
  final List<EmployeeMandates$Query$EmployeeMandates?> list;
  final Function(
    BuildContext context, {
    required String mandateId,
    bool? nullVote,
    bool? whiteVote,
    EmployeeCandidateFragmentMixin$Person? person,
  }) onTap;
  final UserModel user;

  const AlertListCandidatesWidget({
    required this.context,
    required this.list,
    required this.onTap,
    required this.user,
  });

  @override
  State<AlertListCandidatesWidget> createState() =>
      _AlertListCandidatesWidgetState();
}

class _AlertListCandidatesWidgetState extends State<AlertListCandidatesWidget> {
  EmployeeCandidateFragmentMixin$Person? candidate;
  bool whiteVote = false;
  bool nullVote = false;

  @override
  Widget build(BuildContext context) {
    return ZoomIn(
      duration: Duration(milliseconds: 200),
      child: AlertDialog(
        title: Text(
          '${widget.list.first?.description}',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
        contentPadding: EdgeInsets.zero,
        content: Container(
          height: MediaQuery.of(context).size.width,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: ListBody(children: [
              Padding(
                padding: const EdgeInsets.all(paddingSize),
                child: const Text(
                  'Escolha o seu candidato:',
                  textAlign: TextAlign.center,
                ),
              ),
              if (widget.list.isNotEmpty)
                ...widget.list.first!.candidates!.map((e) {
                  return Container(
                    decoration: BoxDecoration(
                        color: e.person == candidate
                            ? primaryColor
                            : Colors.transparent,
                        borderRadius: BorderRadius.circular(4)),
                    margin: EdgeInsets.symmetric(horizontal: paddingSize),
                    padding: EdgeInsets.symmetric(horizontal: paddingSize / 2),
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Text(
                        e.person.name,
                        style: TextStyle(
                          color:
                              e.person == candidate ? Colors.white : textColor,
                        ),
                      ),
                      leading: CircleAvatar(
                        radius: 18,
                        backgroundColor:
                            e.person == candidate ? Colors.white : primaryColor,
                        backgroundImage: e.person.profilePicture != null
                            ? NetworkImage(e.person.profilePicture!)
                            : null,
                        child: Text(
                          e.person.name[0],
                          style: TextStyle(
                            color: e.person == candidate
                                ? textColor
                                : Colors.white,
                          ),
                        ),
                      ),
                      enabled: true,
                      selectedColor: Colors.amber,
                      onTap: () {
                        setState(() {
                          candidate = e.person;
                          nullVote = false;
                          whiteVote = false;
                        });
                      },
                    ),
                  );
                }).toList(),
              Container(
                decoration: BoxDecoration(
                    color: nullVote == true ? primaryColor : Colors.transparent,
                    borderRadius: BorderRadius.circular(4)),
                margin: EdgeInsets.symmetric(horizontal: paddingSize),
                padding: EdgeInsets.symmetric(horizontal: paddingSize / 2),
                child: ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Text(
                    'Nulo',
                    style: TextStyle(
                      color: nullVote == true ? Colors.white : textColor,
                    ),
                  ),
                  leading: Icon(
                    Icons.do_not_disturb_alt_outlined,
                    color: nullVote == true ? Colors.white : dangerColor,
                    size: 40,
                  ),
                  onTap: () {
                    setState(() {
                      candidate =
                          EmployeeCandidateFragmentMixin$Person.fromJson(
                              {'id': '', 'name': ''});
                      nullVote = true;
                      whiteVote = false;
                    });
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color:
                        whiteVote == true ? primaryColor : Colors.transparent,
                    borderRadius: BorderRadius.circular(4)),
                margin: EdgeInsets.symmetric(horizontal: paddingSize),
                padding: EdgeInsets.symmetric(horizontal: paddingSize / 2),
                child: ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Text(
                    'Branco',
                    style: TextStyle(
                      color: whiteVote == true ? Colors.white : textColor,
                    ),
                  ),
                  leading: Icon(
                    Icons.account_circle_rounded,
                    color: whiteVote == true ? Colors.white : textLightColor,
                    size: 40,
                  ),
                  onTap: () {
                    setState(() {
                      candidate =
                          EmployeeCandidateFragmentMixin$Person.fromJson(
                              {'id': '', 'name': ''});
                      nullVote = false;
                      whiteVote = true;
                    });
                  },
                ),
              )
            ]),
          ),
        ),
        actions: [
          TextButton(
            child: const Text('CANCELAR'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          const SizedBox(width: paddingSize),
          ClipRRect(
            borderRadius: BorderRadius.circular(4),
            child: Container(
              color: candidate == null
                  ? textLightColor.withOpacity(0.4)
                  : primaryColor,
              padding: const EdgeInsets.symmetric(horizontal: spaceSize),
              child: TextButton(
                child: Text('VOTAR',
                    style: TextStyle(
                      color: candidate == null ? textLightColor : Colors.white,
                    )),
                onPressed: () async {
                  await AlertWidget.show(AlertAnimateEnum.fade,
                      context: context,
                      title: Text('Votação'),
                      body: [
                        Text('Deseja confirmar seu voto?')
                      ],
                      actions: [
                        TextButton(
                          child: const Text('CANCELAR'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(4),
                          child: Container(
                            color: successColor,
                            padding: const EdgeInsets.symmetric(
                                horizontal: spaceSize),
                            child: TextButton(
                              child: Text('COMFIRMAR',
                                  style: TextStyle(
                                    color: Colors.white,
                                  )),
                              onPressed: () => widget.onTap(
                                widget.context,
                                mandateId: widget.list.first!.mandateId,
                                nullVote: nullVote,
                                whiteVote: whiteVote,
                                person: candidate,
                              ),
                            ),
                          ),
                        )
                      ]);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
