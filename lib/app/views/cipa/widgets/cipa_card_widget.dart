import 'package:flutter/material.dart';
import 'package:quirons_user/app/constants/app_metrics.dart';

import '../../../constants/app_colors.dart';

class CipaCardWidget extends StatelessWidget {
  const CipaCardWidget({
    required this.width,
    required this.type,
    required this.textButton,
    required this.onTap,
  });

  final double width;
  final String type;
  final String textButton;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: paddingSize),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      '$type',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: textColor,
                      ),
                    ),
                    Text(
                      'CIPA',
                      style: TextStyle(
                        fontSize: 12,
                        color: successColor,
                        height: 2,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                InkWell(
                  onTap: onTap,
                  enableFeedback: true,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Container(
                        height: 56,
                        color: primaryColor,
                        child: Center(
                          child: Text(
                            textButton,
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
