import 'package:flutter/material.dart';
import 'package:quirons_user/app/constants/app_metrics.dart';
import 'package:quirons_user/app/graphql/gen/api.graphql.dart';
import 'package:quirons_user/app/utils/exam_nature_enum.dart';
import 'package:quirons_user/app/utils/format_date.dart';

import '../../../constants/app_colors.dart';

class NextEventWidget extends StatelessWidget {
  const NextEventWidget({
    required this.exam,
    required this.ipe,
  });

  final EmployeeConsultations$Query$EmployeeConsultations? exam;
  final EmployeeIpes$Query$EmployeeIpes? ipe;

  Widget cardEvent({
    required String title,
    required String subtitle,
    required DateTime date,
  }) {
    return Card(
      elevation: 4,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          height: 120,
          child: Row(
            children: [
              Container(
                width: 8,
                height: double.infinity,
                color: primaryColor,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(paddingSize),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            title,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            toDescriptiveDateTime(date),
                          )
                        ],
                      ),
                      Text(
                        subtitle,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Próximos eventos',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        if (ipe != null && exam != null)
          Column(
            children: [
              Visibility(
                visible: exam != null,
                child: cardEvent(
                  title: 'EXAME',
                  subtitle: 'Natureza: ${ExamNatureDescription[exam!.nature]}',
                  date: exam!.estimatedDate,
                ),
              ),
              Visibility(
                  visible: ipe != null,
                  child: cardEvent(
                    title: 'TROCA DE EPI',
                    subtitle: ipe!.equipmentDescription,
                    date: ipe!.nextChange!,
                  )),
            ],
          )
        else
          Center(
              child: Padding(
            padding: const EdgeInsets.all(paddingSize),
            child: Text('Não há proximos eventos.'),
          ))
      ],
    );
  }
}
