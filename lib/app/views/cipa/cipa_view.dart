import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:quirons_user/app/utils/show_meet.dart';
import 'package:quirons_user/app/views/cipa/widgets/alert_success_candidate_widget.dart';
import 'package:quirons_user/app/views/cipa/widgets/next_event_widget.dart';
import '../../controllers/cipa_controller.dart';

import '../../widgets/alert_widget.dart';
import '../../constants/app_colors.dart';
import '../../constants/app_metrics.dart';
import 'widgets/alert_list_candidates_widget.dart';
import 'widgets/cipa_card_widget.dart';

class CipaView extends StatelessWidget {
  final cipaController = GetIt.instance.get<CipaController>();

  Future<void> sendMyCandidature(BuildContext context) async {
    await AlertWidget.show(
      AlertAnimateEnum.zoom,
      context: context,
      title: Text('Candidatura'),
      body: [Text('Você deseja realmente candidatar-se?')],
      actions: [
        TextButton(
          child: const Text('CANCELAR'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        const SizedBox(width: paddingSize),
        ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: Container(
            color: successColor,
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: TextButton(
              child: const Text('CONFIRMAR',
                  style: TextStyle(
                    color: Colors.white,
                  )),
              onPressed: () async {
                Navigator.of(context).pop();

                final response = await cipaController.sendMyCanditature();

                if (response) {
                  AlertCandidateSuccessWidget.show(context);
                  await cipaController.init();
                }
              },
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(title: const Text('Início')),
      body: AnimatedBuilder(
          animation: Listenable.merge([
            cipaController.loading,
            cipaController.hasCandidate,
            cipaController.hasVoted,
            cipaController.ipeNextChange,
            cipaController.examEstimate,
          ]),
          builder: (context, __) {
            return cipaController.loading.value
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : SingleChildScrollView(
                    padding: const EdgeInsets.all(paddingSize),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Olá ${cipaController.currentUser.value.name.split(" ")[0]}, ${ShowMeet()}!',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: spaceSize / 2),
                        Visibility(
                          visible: !cipaController.hasCandidate.value,
                          child: CipaCardWidget(
                            width: width,
                            type: 'Candidatura',
                            textButton: 'Quero me candidatar',
                            onTap: () async {
                              await sendMyCandidature(context);
                            },
                          ),
                        ),
                        Visibility(
                          visible: !cipaController.hasVoted.value &&
                              cipaController
                                  .listMandatesToVote.value.isNotEmpty,
                          child: CipaCardWidget(
                            width: width,
                            type: 'Votação',
                            textButton: 'Votar',
                            onTap: () async {
                              await showDialog(
                                context: context,
                                barrierDismissible: true,
                                builder: (context) {
                                  return AlertListCandidatesWidget(
                                    context: context,
                                    user: cipaController.currentUser.value,
                                    list:
                                        cipaController.listMandatesToVote.value,
                                    onTap: cipaController.sendMyVote,
                                  );
                                },
                              );
                            },
                          ),
                        ),
                        SizedBox(height: spaceSize),
                        NextEventWidget(
                          exam: cipaController.examEstimate.value,
                          ipe: cipaController.ipeNextChange.value,
                        )
                      ],
                    ),
                  );
          }),
    );
  }
}
