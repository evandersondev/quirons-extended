import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:quirons_user/app/controllers/risk_controller.dart';
import 'package:quirons_user/app/utils/format_date.dart';
import '../../constants/app_colors.dart';
import '../../graphql/gen/api.graphql.dart';
import 'risk_detail_view.dart';

final riskDegreeLabel = {
  RiskDegree.small: 'Baixo',
  RiskDegree.medium: 'Médio',
  RiskDegree.large: 'Alto',
};

final agentGroupLabel = {
  AgentGroup.chemical: 'Q',
  AgentGroup.physical: 'F',
  AgentGroup.biological: 'B',
  AgentGroup.ergonomic: 'E',
  AgentGroup.mechanical: 'M',
  AgentGroup.dangerous: 'P',
};

final agentGroupLabelColor = {
  'Q': dangerColor,
  'F': successLowColor,
  'B': browColor,
  'E': warningLowColor,
  'M': infoLowColor,
  'P': orangeColor,
};

class RiskView extends StatelessWidget {
  final riskController = GetIt.instance.get<RiskController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Riscos'),
      ),
      body: AnimatedBuilder(
          animation: Listenable.merge(
              [riskController.loading, riskController.riskList]),
          builder: (context, _) {
            return SingleChildScrollView(
              padding: const EdgeInsets.all(12),
              child: Column(
                children: riskController.riskList.value.map((e) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return RiskDetailView(risk: e);
                      }));
                    },
                    enableFeedback: true,
                    child: Card(
                      elevation: 4,
                      margin: const EdgeInsets.only(bottom: 0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(4),
                        child: SizedBox(
                          height: 120,
                          child: Row(
                            children: [
                              Container(
                                height: double.infinity,
                                width: 8,
                                color: agentGroupLabelColor[
                                    agentGroupLabel[e.riskGroup]!],
                              ),
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.only(
                                      top: 12, left: 12, right: 12),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        e.riskAgent,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                        ),
                                      ),
                                      Container(
                                        height: 35,
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 4),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Início',
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.black45,
                                              ),
                                            ),
                                            Text(
                                              dateTimeFormatted(
                                                  e.recognitionDate),
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        height: 35,
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 4),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Grau de risco',
                                              style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.black45,
                                              ),
                                            ),
                                            Text(
                                              riskDegreeLabel[e.riskDegree] ??
                                                  '',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              if (agentGroupLabel[e.riskGroup] != null)
                                SizedBox(
                                  width: 50,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 35,
                                        width: 30,
                                        padding: const EdgeInsets.only(top: 8),
                                        decoration: BoxDecoration(
                                            color: agentGroupLabelColor[
                                                agentGroupLabel[e.riskGroup]!],
                                            borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(15),
                                              bottomRight: Radius.circular(15),
                                            )),
                                        child: Text(
                                          agentGroupLabel[e.riskGroup]!,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }).toList(),
              ),
            );
          }),
    );
  }
}
