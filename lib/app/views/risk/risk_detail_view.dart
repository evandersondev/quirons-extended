import 'package:flutter/material.dart';
import 'package:quirons_user/app/utils/format_date.dart';

import '../../constants/app_colors.dart';
import '../../graphql/gen/api.graphql.dart';
import '../../widgets/list_item_widget.dart';
import '../../widgets/table_widget.dart';
import '../../widgets/two_columns_widget.dart';

final riskDegreeLabel = {
  RiskDegree.small: 'Baixo',
  RiskDegree.medium: 'Médio',
  RiskDegree.large: 'Alto',
};

final riskMapLabel = {
  RiskMap.cipa: 'CIPA',
  RiskMap.sesmt: 'SESMT',
  RiskMap.both: 'Ambos'
};

final riskCategoryLabel = {
  RiskCategory.attention: 'Atenção',
  RiskCategory.critical: 'Crítica',
  RiskCategory.intolerable: 'Intolerável',
  RiskCategory.irrelevant: 'Irrelevante',
};

final riskProbalityLabel = {
  RiskProbability.low: 'Baixa',
  RiskProbability.intermediate: 'Intermediária',
  RiskProbability.medium: 'Mediana',
  RiskProbability.high: 'Alta',
  RiskProbability.veryHigh: 'Altíssima',
};

final severityLabel = {
  RiskSeverity.mild: 'Leve',
  RiskSeverity.intermediate: 'Intermediária',
  RiskSeverity.moderate: 'Moderada',
  RiskSeverity.severe: 'Grave',
  RiskSeverity.verySevere: 'Gravíssima',
};

class RiskDetailView extends StatelessWidget {
  RiskDetailView({required this.risk});

  final EmployeeRisks$Query$EmployeeRisks risk;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Risco'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(12),
        child: Column(
          children: [
            TwoColumnsWidget(
              context,
              left: ListItemWidget(
                title: 'Data de reconhecimento',
                subtitle: dateTimeFormatted(risk.recognitionDate),
              ),
              right: ListItemWidget(
                title: 'Tipo de ação',
                subtitle: risk.measureType.toString(),
              ),
            ),
            ListItemWidget(
              title: 'Agente',
              subtitle: risk.riskAgent,
            ),
            ListItemWidget(
              title: 'Fonte geradora',
              subtitle: risk.measureType.toString(),
            ),
            ListItemWidget(
              title: 'Ambiente físico',
              subtitle: risk.physicalEnvironment,
            ),
            ListItemWidget(
              title: 'Limite de tolerância',
              subtitle: risk.actionLevel.toString(),
            ),
            TwoColumnsWidget(
              context,
              left: ListItemWidget(
                title: 'Tempo de exposição',
                subtitle: risk.repetition,
              ),
              right: ListItemWidget(
                title: 'Intervalo',
                subtitle: risk.interval,
              ),
            ),
            TwoColumnsWidget(
              context,
              left: ListItemWidget(
                title: 'Grau de risco',
                subtitle: riskDegreeLabel[risk.riskDegree] ?? '',
              ),
              right: ListItemWidget(
                title: 'Mapa de risco',
                subtitle: riskMapLabel[risk.riskMap],
              ),
            ),
            Row(
              children: [
                SwitchCustomWidget(value: risk.needIpe ?? false),
                Text('Necessita de EPI?'),
              ],
            ),
            Row(
              children: [
                SwitchCustomWidget(value: risk.needCpe ?? false),
                Text('Necessita de EPC?'),
              ],
            ),
            Row(
              children: [
                SwitchCustomWidget(value: risk.protectionMeasure),
                Text('Medidas de proteção'),
              ],
            ),
            Row(
              children: [
                SwitchCustomWidget(value: risk.workingCondition),
                Text('Condições de funcionamento'),
              ],
            ),
            Row(
              children: [
                SwitchCustomWidget(value: risk.uninterruptedUse),
                Text('Uso ininterrupto'),
              ],
            ),
            Row(
              children: [
                SwitchCustomWidget(value: risk.expirationDate),
                Text('Prazo de validade'),
              ],
            ),
            Row(
              children: [
                SwitchCustomWidget(value: risk.exchangePeriodicity),
                Text('Periodicidade de troca'),
              ],
            ),
            Row(
              children: [
                SwitchCustomWidget(value: risk.sanitation),
                Text('Higienização'),
              ],
            ),
            ListItemWidget(
              title: 'Categoria',
              subtitle: riskCategoryLabel[risk.category],
            ),
            TwoColumnsWidget(
              context,
              left: ListItemWidget(
                title: 'Probabilidade',
                subtitle: riskProbalityLabel[risk.probability],
              ),
              right: ListItemWidget(
                title: 'Severidade',
                subtitle: severityLabel[risk.severity] ?? '',
              ),
            ),
            TableWidget(
              title: 'Medidas de Controle',
              header: ['Medida de Controle', 'Efetivo?'],
              body: [
                ...risk.controlMeasures
                    .map((e) => [e.description, '-'])
                    .toList()
              ],
            ),
            const SizedBox(height: 8),
            TableWidget(
              title: 'Equipamento',
              header: ['Equipamento', 'Dias para Substituição', 'Lista CA'],
              body: [
                ...risk.ipes.map((e) => [e.description, '', '']).toList()
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SwitchCustomWidget extends StatelessWidget {
  const SwitchCustomWidget({
    Key? key,
    required this.value,
  }) : super(key: key);

  final bool value;

  @override
  Widget build(BuildContext context) {
    return Switch(
      value: value,
      onChanged: null,
      activeTrackColor: dangerColor,
      activeColor: successColor,
      inactiveThumbColor: !value ? Colors.white : textColor.withOpacity(0.5),
      inactiveTrackColor: textColor.withOpacity(0.2),
    );
  }
}
