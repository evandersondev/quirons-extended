import 'package:flutter/material.dart';

import '../../graphql/gen/api.graphql.dart';
import './widgets/item_profile_widget.dart';
import '../../constants/app_colors.dart';

class OrganizationView extends StatelessWidget {
  OrganizationView({required this.user});

  final EmployeeInfo$Query$EmployeeInfo user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Informações Organizacionais'),
      ),
      body: Column(
        children: [
          SizedBox(height: 20),
          ItemProfileWidget(
            title: 'Matricula',
            subtitle: user.registration ?? '',
            leading: Icon(
              Icons.badge_outlined,
              size: 30,
              color: primaryColor,
            ),
          ),
          Divider(),
          ItemProfileWidget(
            title: 'Função',
            subtitle: user.workShift?.description ?? '',
            leading: Icon(
              Icons.business_center_rounded,
              size: 30,
              color: primaryColor,
            ),
          ),
          Divider(),
          ItemProfileWidget(
            title: 'Cargo',
            subtitle: user.occupation?.description ?? '',
            leading: Icon(
              Icons.work,
              size: 30,
              color: primaryColor,
            ),
          ),
          Divider(),
          ItemProfileWidget(
            title: 'Centro de Custo',
            subtitle: user.costCenter?.description ?? '',
            leading: Icon(
              Icons.bar_chart_outlined,
              size: 30,
              color: primaryColor,
            ),
          ),
          Divider(),
          ItemProfileWidget(
            title: 'Departamento',
            subtitle: user.department?.description ?? '',
            leading: Icon(
              Icons.people_alt,
              size: 30,
              color: primaryColor,
            ),
          ),
        ],
      ),
    );
  }
}
