import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:get_it/get_it.dart';
import 'package:quirons_user/app/views/login/login_view.dart';
import '../../controllers/profile_controller.dart';
import '../../services/auth_service.dart';

import './widgets/item_profile_widget.dart';
import '../../constants/app_colors.dart';
import 'organization_view.dart';
import 'personal_data_view.dart';
import '../../widgets/webview_page.dart';
import 'widgets/profile_options.dart';

class ProfileView extends StatefulWidget {
  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  final profileController = GetIt.instance.get<ProfileController>();
  bool loading = false;
  final auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Perfil'),
        actions: [
          IconButton(
            icon: const Icon(Icons.more_vert),
            onPressed: () => Get.bottomSheet(ProfileOptions(
              onLogout: () async {
                await auth.clearToken();

                Get.offAll(() => LoginView());
              },
            )),
          )
        ],
      ),
      body: AnimatedBuilder(
          animation: Listenable.merge([
            profileController.userInfo,
            profileController.userInfo,
          ]),
          builder: (_, __) {
            final user = profileController.userInfo.value;

            if (profileController.loading.value) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }

            return SingleChildScrollView(
              padding: const EdgeInsets.only(
                  left: 12, right: 12, top: 8.0, bottom: 90),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  const SizedBox(height: 30),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                          padding: const EdgeInsets.all(2),
                          height: 60,
                          width: 60,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(30),
                            child: Image.network(
                              user?.profilePicture ?? '',
                              errorBuilder: (_, error, __) => const Center(
                                  child: Icon(
                                Icons.add_a_photo_outlined,
                                color: Colors.white,
                                size: 32,
                              )),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 8),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            user?.name ?? '',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: primaryColor,
                            ),
                          ),
                          Text(
                            user?.occupation?.description ?? '',
                            style: TextStyle(
                              color: Colors.black38,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  const SizedBox(height: 30),
                  const Divider(),
                  ItemProfileWidget(
                    title: 'Perfil',
                    subtitle: 'Informações Pessoais',
                    leading: const Icon(
                      Icons.person,
                      size: 30,
                      color: primaryColor,
                    ),
                    trailing: const Icon(
                      Icons.keyboard_arrow_right_outlined,
                      size: 30,
                      color: primaryColor,
                    ),
                    onTap: () => Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return PersonalDataView(user: user!);
                    })),
                  ),
                  const Divider(),
                  ItemProfileWidget(
                    title: 'Organização',
                    subtitle: 'Informações Organizacionais',
                    leading: const Icon(
                      Icons.business_outlined,
                      size: 30,
                      color: primaryColor,
                    ),
                    trailing: const Icon(
                      Icons.keyboard_arrow_right_outlined,
                      size: 30,
                      color: primaryColor,
                    ),
                    onTap: () => Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return OrganizationView(
                        user: user!,
                      );
                    })),
                  ),
                  const Divider(),
                  ItemProfileWidget(
                    title: 'Utilização',
                    subtitle: 'Política de privacidade',
                    leading: const Icon(
                      Icons.lock,
                      size: 30,
                      color: primaryColor,
                    ),
                    trailing: const Icon(
                      Icons.open_in_new_rounded,
                      size: 30,
                      color: primaryColor,
                    ),
                    onTap: () => Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return const WebViewPage(
                          url:
                              'https://quirons.com.br/politica-de-privacidade/');
                    })),
                  ),
                  // const Divider(),
                  // ItemProfileWidget(
                  //   title: 'Tema',
                  //   subtitle: 'Modo Escuro',
                  //   trailing: Switch(
                  //     value: false,
                  //     activeColor: dangerColor,
                  //     onChanged: (_) {},
                  //   ),
                  //   leading: const Icon(
                  //     Icons.brightness_2_rounded,
                  //     color: primaryColor,
                  //     size: 30,
                  //   ),
                  // ),
                ],
              ),
            );
          }),
    );
  }
}
