import 'package:flutter/material.dart';

import '../../graphql/gen/api.graphql.dart';
import './widgets/item_profile_widget.dart';
import '../../constants/app_colors.dart';

final userGenderLabel = {Gender.female: 'Feminino', Gender.male: 'Masculino'};

final educationLevelLabel = {
  EducationLevel.doctorateDegree: 'Doutorado',
  EducationLevel.elementarySchool: 'Ensino Fundamental',
  EducationLevel.highSchool: 'Ensino médio',
  EducationLevel.mastersDegree: 'Mestrado',
  EducationLevel.universityGraduate: 'Graduação',
};

class PersonalDataView extends StatelessWidget {
  PersonalDataView({required this.user});

  final EmployeeInfo$Query$EmployeeInfo user;

  @override
  Widget build(BuildContext context) {
    const iconColor = primaryColor;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Informações Pessoais'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(bottom: 20),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: GestureDetector(
                onTap: () {},
                child: Container(
                  padding: const EdgeInsets.all(2),
                  height: 120,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(60),
                    child: Image.network(
                      user.profilePicture ?? '',
                      errorBuilder: (_, error, __) => const Center(
                          child: Icon(
                        Icons.add_a_photo_outlined,
                        color: Colors.white,
                        size: 32,
                      )),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            // const Divider(),
            ItemProfileWidget(
              title: 'Nome',
              subtitle: user.name,
              leading: Icon(
                Icons.person,
                size: 30,
                color: iconColor,
              ),
            ),
            const Divider(),
            ItemProfileWidget(
              title: 'E-mail',
              subtitle: user.email ?? '',
              leading: Icon(
                Icons.email,
                size: 30,
                color: iconColor,
              ),
            ),
            const Divider(),
            ItemProfileWidget(
              title: 'Sexo',
              subtitle: userGenderLabel[user.gender]!,
              leading: Icon(
                Icons.emoji_people_rounded,
                size: 30,
                color: iconColor,
              ),
            ),
            const Divider(),
            ItemProfileWidget(
              title: 'Endereço',
              subtitle: user.address ?? '',
              leading: Icon(
                Icons.place,
                size: 30,
                color: iconColor,
              ),
            ),
            const Divider(),
            ItemProfileWidget(
              title: 'CPF',
              subtitle: user.individualRegistration,
              leading: Icon(
                Icons.credit_card,
                size: 30,
                color: iconColor,
              ),
            ),
            const Divider(),
            ItemProfileWidget(
              title: 'Escolaridade',
              subtitle: educationLevelLabel[user.educationLevel] ?? '',
              leading: Icon(
                Icons.school,
                size: 30,
                color: iconColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
