import 'package:flutter/material.dart';
import '../../../constants/app_colors.dart';
import '../../../widgets/bottom_sheet_widget.dart';

class ProfileOptions extends StatelessWidget {
  const ProfileOptions({required this.onLogout});
  final VoidCallback onLogout;

  @override
  Widget build(BuildContext context) {
    return BottomSheetWidget(
      child: Wrap(children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListTile(
                leading: const Icon(
                  Icons.logout,
                  color: primaryColor,
                ),
                title: const Text(
                  'Logout',
                  style: TextStyle(
                    color: Colors.black87,
                  ),
                ),
                onTap: onLogout,
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
