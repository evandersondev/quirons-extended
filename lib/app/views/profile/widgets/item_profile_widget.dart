import 'package:flutter/material.dart';
import '../../../constants/app_colors.dart';

class ItemProfileWidget extends StatelessWidget {
  const ItemProfileWidget({
    required this.leading,
    required this.title,
    required this.subtitle,
    this.onTap,
    this.trailing,
  });

  final Widget leading;
  final String title;
  final String subtitle;
  final Function()? onTap;
  final Widget? trailing;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      visualDensity: VisualDensity.compact,
      leading: SizedBox(
        height: double.infinity,
        child: leading,
      ),
      onTap: onTap,
      trailing: trailing,
      title: Text(
        title,
        style: const TextStyle(
          color: primaryColor,
          fontSize: 12,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        subtitle,
        style: const TextStyle(
          color: primaryColor,
        ),
      ),
    );
  }
}
