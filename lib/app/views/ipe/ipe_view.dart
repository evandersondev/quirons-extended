import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:quirons_user/app/controllers/ipe_controller.dart';
import 'package:quirons_user/app/utils/format_date.dart';
import 'package:quirons_user/app/views/ipe/ipe_detail_view.dart';

import '../../constants/app_colors.dart';

class IpeView extends StatelessWidget {
  final ipeController = GetIt.instance.get<IpeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("EPI's")),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(12.0),
        child: AnimatedBuilder(
            animation: Listenable.merge(
                [ipeController.loading, ipeController.ipesList]),
            builder: (context, __) {
              return ipeController.loading.value
                  ? Center(child: CircularProgressIndicator())
                  : Column(
                      children: ipeController.ipesList.value.map((ipe) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return IpeDetailView(ipe: ipe);
                            }));
                          },
                          child: Card(
                            elevation: 4,
                            color: Colors.white,
                            margin: const EdgeInsets.only(bottom: 12),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(4),
                              child: SizedBox(
                                height: 120,
                                child: Row(
                                  children: [
                                    Container(
                                      width: 8,
                                      height: double.infinity,
                                      color: primaryColor,
                                    ),
                                    Expanded(
                                        child: Container(
                                      padding: const EdgeInsets.all(12),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'C.A. ${ipe.caCode ?? ''}',
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  if (ipe.date != null)
                                                    SizedBox(
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            '${dateTimeFormatted(ipe.date!)} - ${toTime(ipe.date!)}',
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                ],
                                              ),
                                              const SizedBox(height: 8),
                                              Text(ipe.equipmentDescription,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    color: textColor,
                                                  )),
                                            ],
                                          ),
                                          if (ipe.nextChange != null)
                                            Text(
                                                'Troca: ${toDescriptiveDateTime(ipe.nextChange!)}  ás ${toTime(ipe.nextChange!)}'),
                                        ],
                                      ),
                                    )),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      }).toList(),
                    );
            }),
      ),
    );
  }
}
