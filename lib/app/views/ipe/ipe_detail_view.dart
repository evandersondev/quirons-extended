import 'package:flutter/material.dart';
import 'package:quirons_user/app/graphql/gen/api.graphql.dart';
import 'package:quirons_user/app/utils/format_date.dart';
import '../../constants/app_colors.dart';

import '../../widgets/list_item_widget.dart';
import '../../widgets/two_columns_widget.dart';

final reasonDescriptive = {
  DeliveryReason.admissional: 'Admissional',
  DeliveryReason.defect: 'Defeito',
  DeliveryReason.demissional: 'Demissional',
  DeliveryReason.dielectricTest: 'Teste dielétrico',
  DeliveryReason.functionChange: 'Mundaça de função',
  DeliveryReason.other: 'Outro',
  DeliveryReason.sanitation: 'Saneamento',
  DeliveryReason.theft: 'Roubo',
  DeliveryReason.wear: 'Vestir',
  DeliveryReason.loss: 'Perda',
};

class IpeDetailView extends StatelessWidget {
  const IpeDetailView({required this.ipe});

  final EmployeeIpes$Query$EmployeeIpes ipe;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Detalhes do EPI')),
      body: SingleChildScrollView(
        padding: const EdgeInsets.fromLTRB(12, 12, 12, 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              ipe.equipmentDescription,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
                color: textColor,
              ),
            ),
            if (ipe.caCode != null)
              ListItemWidget(
                title: 'Certificado de aprovação',
                subtitle: ipe.caCode,
              ),
            ListItemWidget(
              title: 'Expirado em',
              subtitle: dateTimeFormatted(ipe.nextChange!), // '22/09/2020',
            ),
            TwoColumnsWidget(
              context,
              left: ListItemWidget(
                title: 'Tipo',
                subtitle: ipe.reason == null
                    ? 'Outro'
                    : reasonDescriptive[ipe.reason],
              ),
              right: ListItemWidget(
                title: 'Data',
                subtitle: ipe.date == null ? '' : dateTimeFormatted(ipe.date!),
              ),
            ),
            ListItemWidget(
              title: 'Última manutenção do EPI',
              subtitle: ipe.lastMaintenance == null
                  ? ''
                  : dateTimeFormatted(ipe.lastMaintenance!),
            ),
            TwoColumnsWidget(context,
                left: ListItemWidget(
                  title: 'Quantidade',
                  subtitle: '${ipe.amount?.toInt() ?? 0}',
                ),
                right: Row(
                  children: [
                    Switch(
                      value: ipe.effective ?? false,
                      onChanged: null,
                    ),
                    Text('Eficaz'),
                  ],
                )),
            ListItemWidget(
              title: 'Motivo da entrega',
              subtitle: 'Desgaste',
            ),
            ListItemWidget(
              title: 'Observação',
              subtitle: ipe.observation,
            ),
          ],
        ),
      ),
    );
  }
}
