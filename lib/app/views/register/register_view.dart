import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'package:flutter/material.dart';
import 'package:quirons_user/app/utils/select_birthdate.dart';

import '../../repositories/register_repository.dart';
import '../../utils/input_formatters.dart';
import '../../controllers/resgister_controller.dart';
import '../../assets/assets.dart';
import '../../graphql/gen/api.graphql.dart';

import '../../constants/app_colors.dart';
import '../../constants/app_metrics.dart';
import '../../widgets/two_columns_widget.dart';

class RegisterView extends StatelessWidget {
  final registerController = RegisterController(RegisterRepository());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cadastrar'),
        actions: [
          Container(
            width: 115,
            padding: EdgeInsets.symmetric(horizontal: paddingSize),
            child: Image.asset(quironsLogo),
          )
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(paddingSize),
        child: Form(
          key: registerController.formKey,
          child: Column(
            children: [
              const SizedBox(height: paddingSize),
              TextFormField(
                readOnly: true,
                controller: registerController.organizationController,
                onTap: registerController.getOrganization,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  hintText: 'Organização',
                  labelText: 'Organização',
                  suffixIcon: Icon(
                    Icons.qr_code,
                    color: registerController.organizationController.text != ''
                        ? successColor
                        : textLightColor,
                  ),
                ),
              ),
              const SizedBox(height: paddingSize),
              TextFormField(
                controller: registerController.registrationController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  hintText: 'Matrícula',
                  labelText: 'Matrícula',
                ),
              ),
              const SizedBox(height: paddingSize),
              TextFormField(
                controller: registerController.cpfController,
                validator: (value) {
                  return CPFValidator.isValid(value) ? null : 'CPF inválido';
                },
                inputFormatters: [cpfFormatter],
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  hintText: 'CPF',
                  labelText: 'CPF',
                ),
              ),
              const SizedBox(height: paddingSize),
              TextFormField(
                controller: registerController.birthDateController,
                readOnly: true,
                inputFormatters: [birthDateFormatter],
                keyboardType: TextInputType.number,
                onTap: () async {
                  final date = await selectBirthdate(context);

                  if (date != null) {
                    registerController.updateBirthDate(date);
                  }
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  hintText: 'Data de nascimento',
                  labelText: 'Data de nascimento',
                  suffixIcon: const Icon(Icons.calendar_today_rounded),
                ),
              ),
              const SizedBox(height: paddingSize),
              ValueListenableBuilder<bool>(
                  valueListenable: registerController.showPassword,
                  builder: (_, showPassword, __) {
                    return TextField(
                      controller: registerController.passwordController,
                      obscureText: !registerController.showPassword.value,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(4),
                        ),
                        hintText: 'Senha',
                        labelText: 'Senha',
                        suffixIcon: IconButton(
                          onPressed: registerController.shouldShowPassword,
                          icon: Icon(
                            showPassword
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        ),
                      ),
                    );
                  }),
              const SizedBox(height: paddingSize),
              ValueListenableBuilder<EmployeeOperationType>(
                  valueListenable: registerController.operationType,
                  builder: (_, type, __) {
                    return TwoColumnsWidget(
                      context,
                      left: ListTile(
                        title: const Text('Nova Senha'),
                        contentPadding: EdgeInsets.zero,
                        visualDensity: VisualDensity.compact,
                        minLeadingWidth: 20,
                        leading: SizedBox(
                          width: 20,
                          child: Radio(
                            value: EmployeeOperationType.create,
                            groupValue: type,
                            onChanged: (value) {
                              registerController.updateOperationType(
                                  value as EmployeeOperationType);
                            },
                          ),
                        ),
                      ),
                      right: ListTile(
                        title: const Text('Alterar Senha'),
                        contentPadding: EdgeInsets.zero,
                        visualDensity: VisualDensity.compact,
                        minLeadingWidth: 20,
                        leading: SizedBox(
                          width: 20,
                          child: Radio(
                            value: EmployeeOperationType.change,
                            groupValue: type,
                            onChanged: (value) {
                              registerController.updateOperationType(
                                  value as EmployeeOperationType);
                            },
                          ),
                        ),
                      ),
                    );
                  }),
              const SizedBox(height: paddingSize),
              InkWell(
                onTap: () async {
                  await registerController.handleRegisterSubmit(context);
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(4),
                  child: Container(
                    color: primaryColor,
                    width: double.infinity,
                    height: 56,
                    child: const Center(
                      child: Text(
                        'CADASTRAR',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
