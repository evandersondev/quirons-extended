import 'package:flutter/material.dart';
import 'ipe/ipe_view.dart';

import 'exam/exam_view.dart';
import 'risk/risk_view.dart';
import '../constants/app_colors.dart';
import 'cipa/cipa_view.dart';
import 'profile/profile_view.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  final pageController = PageController();
  int pageIndex = 0;

  final listPages = [
    CipaView(),
    ExamView(),
    IpeView(),
    RiskView(),
    ProfileView(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: listPages[pageIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: (value) {
          setState(() {
            pageIndex = value;
          });
        },
        currentIndex: pageIndex,
        backgroundColor: Colors.white,
        selectedLabelStyle: const TextStyle(color: primaryColor),
        unselectedLabelStyle: const TextStyle(color: Colors.grey),
        selectedFontSize: 12,
        unselectedFontSize: 12,
        items: [
          BottomNavigationBarItem(
              icon: Image.asset(
                'lib/app/assets/icons/home_icon.png',
                color: pageIndex == 0 ? primaryColor : Colors.grey,
              ),
              label: 'Inicio'),
          BottomNavigationBarItem(
              icon: Image.asset(
                'lib/app/assets/icons/calendar_icon.png',
                color: pageIndex == 1 ? primaryColor : Colors.grey,
              ),
              label: 'Exames'),
          BottomNavigationBarItem(
              icon: Image.asset(
                'lib/app/assets/icons/epi_icon.png',
                color: pageIndex == 2 ? primaryColor : Colors.grey,
              ),
              label: "EPI's"),
          BottomNavigationBarItem(
              icon: Image.asset(
                'lib/app/assets/icons/risk_icon.png',
                color: pageIndex == 3 ? primaryColor : Colors.grey,
              ),
              label: 'Riscos'),
          BottomNavigationBarItem(
              icon: Image.asset(
                'lib/app/assets/icons/profile_icon.png',
                color: pageIndex == 4 ? primaryColor : Colors.grey,
              ),
              label: 'Perfil'),
        ],
      ),
    );
  }
}
