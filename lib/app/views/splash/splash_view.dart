import 'package:flutter/material.dart';
import '../../assets/assets.dart';
import '../../../../quirons_user_app_controller.dart';

class SplashView extends StatefulWidget {
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  final quironsAppController = QuironsUserAppController();

  @override
  void initState() {
    super.initState();

    initAsync();
  }

  void initAsync() async {
    await quironsAppController.registerServices();

    Future.delayed(const Duration(seconds: 2), () {
      quironsAppController.decideInitialRoute();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
          child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.7,
        child: Image.asset(
          quironsLogoType,
          fit: BoxFit.contain,
        ),
      )),
    );
  }
}
