import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:quirons_user/app/controllers/exam_controller.dart';
import 'package:quirons_user/app/utils/format_date.dart';
import 'package:quirons_user/app/views/exam/exam_detail_view.dart';
import '../../constants/app_colors.dart';

enum StatusEnum { done, late, waiting }

final statusColor = {
  StatusEnum.done: [successLightColor, successLowColor],
  StatusEnum.late: [warningLightColor, warningLowColor],
  StatusEnum.waiting: [infoLightColor, infoLowColor],
};

class ExamView extends StatefulWidget {
  @override
  State<ExamView> createState() => _ExamViewState();
}

class _ExamViewState extends State<ExamView> {
  final examController = GetIt.instance.get<ExamController>();

  Widget buildBar(BuildContext context, {int? amoutExams}) {
    return DecoratedBox(
      decoration: const BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            color: Colors.white,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: examController.loading.value
                ? const SizedBox(
                    width: 18,
                    height: 18,
                    child: CircularProgressIndicator(
                      color: primaryColor,
                      strokeWidth: 2.5,
                    ),
                  )
                : Text(
                    '${amoutExams ?? 0} Exame${amoutExams == 1 ? '' : 's'}',
                    style: TextStyle(
                      color: textColor,
                    ),
                  ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Exames'),
        elevation: 1,
      ),
      body: AnimatedBuilder(
          animation: Listenable.merge(
              [examController.loading, examController.examsList]),
          builder: (context, __) {
            return examController.loading.value
                ? Center(child: CircularProgressIndicator())
                : SingleChildScrollView(
                    child: Column(
                      children: [
                        buildBar(context,
                            amoutExams: examController.examsList.value.length),
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                              children: examController.examsList.value.map((e) {
                            final date = e.endDate ?? e.estimatedDate;
                            final labelDate =
                                e.endDate != null ? 'Atendido: ' : 'Previsão: ';
                            final labelEstimated = e.endDate != null
                                ? 'Tempo realizado: '
                                : 'Tempo estimado: ';
                            final startDateFormatted = dateTimeFormatted(date);
                            final status = e.endDate != null
                                ? StatusEnum.done
                                : e.estimatedDate.isBefore(DateTime.now())
                                    ? StatusEnum.late
                                    : StatusEnum.waiting;

                            return InkWell(
                              onTap: () {
                                Navigator.of(context)
                                    .push(MaterialPageRoute(builder: (context) {
                                  return ExamDetailView(
                                    exam: e,
                                  );
                                }));
                              },
                              enableFeedback: true,
                              child: Card(
                                margin: const EdgeInsets.only(bottom: 12),
                                color: statusColor[status]![0],
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(4),
                                  child: SizedBox(
                                    height: 120,
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 8,
                                          height: double.infinity,
                                          color: statusColor[status]![1],
                                        ),
                                        Expanded(
                                            child: Container(
                                          padding: const EdgeInsets.all(12),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      const Text(
                                                        'Médico ou Credenciado',
                                                        style: TextStyle(
                                                          fontSize: 10,
                                                          color: Colors.black45,
                                                        ),
                                                      ),
                                                      Text(
                                                        e.doctorName,
                                                        style: const TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: const [
                                                      Text(
                                                        'Natureza',
                                                        style: TextStyle(
                                                          fontSize: 10,
                                                          color: Colors.black45,
                                                        ),
                                                      ),
                                                      Text(
                                                        'Pontual',
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Expanded(
                                                    child: SizedBox(
                                                      height: 35,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: const [
                                                          Text(
                                                            'Motivo',
                                                            style: TextStyle(
                                                              fontSize: 10,
                                                              color: Colors
                                                                  .black45,
                                                            ),
                                                          ),
                                                          Text(
                                                            'Exame Periódico',
                                                            style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Text(
                                                        labelDate,
                                                        style: const TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                      Text(startDateFormatted),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: [
                                                      Text(
                                                        labelEstimated,
                                                        style: const TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                      Text(
                                                          '${e.estimatedTime!.toInt()}m'),
                                                    ],
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        )),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }).toList()),
                        )
                      ],
                    ),
                  );
          }),
    );
  }
}
