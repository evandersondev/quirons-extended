import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:quirons_user/app/utils/format_date.dart';

import '../../constants/app_colors.dart';
import '../../constants/app_metrics.dart';
import '../../graphql/gen/api.graphql.dart';
import '../../widgets/list_item_widget.dart';
import '../../widgets/table_widget.dart';
import '../../widgets/two_columns_widget.dart';

enum StatusEnum { done, late, waiting }

final aggravationLabel = {
  ConsultationType.occupational: 'Ocupacional',
  ConsultationType.clinical: 'Clínico',
  ConsultationType.urgency: 'Urgência',
  ConsultationType.qualityOfLife: 'Qualidade de vida',
};

final statusLabel = {
  StatusEnum.done: 'Realizado',
  StatusEnum.late: 'Atrasado',
  StatusEnum.waiting: 'Agendado',
};

final statusColor = {
  StatusEnum.done: successLowColor,
  StatusEnum.late: warningLowColor,
  StatusEnum.waiting: infoLowColor,
};

final natureLabel = {
  ConsultationNature.admissional: 'Adminissional',
  ConsultationNature.backToWork: 'Volta ao trabalho',
  ConsultationNature.changeOfFunction: 'Mudança de função',
  ConsultationNature.dismissal: 'Demissional',
  ConsultationNature.periodic: 'Periódico',
  ConsultationNature.spotMonitoring: 'Monitoramento local',
};

class ExamDetailView extends StatelessWidget {
  final EmployeeConsultations$Query$EmployeeConsultations exam;

  ExamDetailView({required this.exam});

  @override
  Widget build(BuildContext context) {
    final status = exam.endDate != null
        ? StatusEnum.done
        : exam.estimatedDate.isBefore(DateTime.now())
            ? StatusEnum.late
            : StatusEnum.waiting;
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Detalhes da consulta',
          style: TextStyle(color: textColor),
        ),
        iconTheme: const IconThemeData(color: textColor),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            BadgeWidget(type: exam.type, status: status),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    visualDensity: VisualDensity.compact,
                    contentPadding: EdgeInsets.zero,
                    title: const Text(
                      'Médico ou Credenciado',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Text(exam.doctorName),
                    leading: Container(
                        height: double.infinity,
                        width: spaceSize * 2,
                        padding: const EdgeInsets.all(4),
                        child: SvgPicture.asset(
                          'lib/app/assets/icons/doctor.svg',
                          color: textColor,
                          fit: BoxFit.contain,
                        )),
                  ),
                  TwoColumnsWidget(
                    context,
                    left: ListItemWidget(
                      title: 'Motivo',
                      subtitle: exam.reason,
                    ),
                    right: ListItemWidget(
                      title: 'Natureza',
                      subtitle: natureLabel[exam.nature],
                    ),
                  ),
                  TwoColumnsWidget(
                    context,
                    left: ListItemWidget(
                      title: 'Previsão de início',
                      subtitle: dateTimeFormatted(exam.estimatedDate),
                    ),
                    right: ListItemWidget(
                      title: 'Tempo estimado',
                      subtitle: '${exam.estimatedTime!.toInt()}m',
                    ),
                  ),
                  // if (exam.endDate != null)
                  if (exam.endDate != null)
                    RichText(
                        text: TextSpan(
                            text: 'Atendido no dia ',
                            style: TextStyle(
                              color: textColor,
                              fontSize: 16,
                            ),
                            children: [
                          TextSpan(
                              text: dateTimeFormatted(exam.endDate!),
                              style: TextStyle(
                                color: successColor,
                                fontWeight: FontWeight.bold,
                              )),
                          TextSpan(
                              text: ' das',
                              style: TextStyle(
                                color: textColor,
                                fontWeight: FontWeight.normal,
                              )),
                          TextSpan(
                              text: toTime(exam.startDate!),
                              style: TextStyle(
                                color: successColor,
                                fontWeight: FontWeight.bold,
                              )),
                          TextSpan(
                              text: ' às ',
                              style: TextStyle(
                                color: textColor,
                                fontWeight: FontWeight.normal,
                              )),
                          TextSpan(
                              text: toTime(exam.endDate!),
                              style: TextStyle(
                                color: successColor,
                                fontWeight: FontWeight.bold,
                              ))
                        ])),
                ],
              ),
            ),
            const SizedBox(height: spaceSize),
            TableWidget(
              header: ['Exames complementares'],
              body: [
                ...exam.exams.map((e) => [e.name]).toList(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class BadgeWidget extends StatelessWidget {
  const BadgeWidget({required this.type, required this.status});

  final ConsultationType type;
  final StatusEnum status;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Row(
        children: [
          Container(
            width: MediaQuery.of(context).size.width / 2,
            height: 56,
            color: primaryColor,
            child: Center(
              child: Text(
                '${aggravationLabel[type]}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Container(
            height: 56,
            width: MediaQuery.of(context).size.width / 2,
            color: statusColor[status],
            child: Center(
              child: Text(
                '${statusLabel[status]}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
