import 'package:flutter/material.dart';
import '../../../constants/app_colors.dart';
import '../../../assets/assets.dart';

class HeaderWidget extends StatefulWidget {
  final bool isGrowing;

  const HeaderWidget({required this.isGrowing});

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  final double startHeaderSize = 240.0;
  final double endHeaderSize = 92.0;

  final double startLogoSize = 260.0;
  final double endLogoSize = 140.0;

  final Alignment startLogoAlignment = Alignment.center;
  final Alignment endLogoAlignment = Alignment.centerLeft;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AnimatedContainer(
          curve: Curves.linear,
          duration: const Duration(milliseconds: 300),
          height: widget.isGrowing ? endHeaderSize : startHeaderSize,
          width: MediaQuery.of(context).size.width,
          color: primaryColor,
          child: AnimatedAlign(
            curve: Curves.linear,
            duration: const Duration(milliseconds: 300),
            alignment: widget.isGrowing ? endLogoAlignment : startLogoAlignment,
            child: AnimatedContainer(
              curve: Curves.linear,
              duration: const Duration(milliseconds: 300),
              width: widget.isGrowing ? endLogoSize : startLogoSize,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 10,
                      horizontal: 15,
                    ),
                    child: Image.asset(
                      quironsUserLogoWhite,
                      width: widget.isGrowing ? endLogoSize : startLogoSize,
                      fit: BoxFit.contain,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
