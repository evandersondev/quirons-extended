import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/route_manager.dart';
import '../../repositories/login_repository.dart';
import '../../utils/input_formatters.dart';
import '../register/register_view.dart';
import '../../controllers/login_controller.dart';

import '../../constants/app_colors.dart';
import '../../constants/app_metrics.dart';
import 'widgets/header_widget.dart';

class LoginView extends StatelessWidget {
  final loginController = LoginController(LoginRepository());

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark,
      ),
    );

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 0,
        backgroundColor: primaryColor,
      ),
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          ValueListenableBuilder<bool>(
              valueListenable: loginController.isGrowing,
              builder: (_, value, __) {
                return HeaderWidget(isGrowing: value);
              }),
          SingleChildScrollView(
            padding: const EdgeInsets.all(paddingSize),
            child: Form(
              key: loginController.formKey,
              child: Column(
                children: [
                  const SizedBox(height: paddingSize),
                  TextFormField(
                    readOnly: true,
                    controller: loginController.organizationController,
                    onTap: loginController.getOrganization,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4),
                      ),
                      hintText: 'Organização',
                      labelText: 'Organização',
                      suffixIcon: Icon(
                        Icons.qr_code,
                        color: loginController.organizationController.text != ''
                            ? successColor
                            : textLightColor,
                      ),
                    ),
                  ),
                  const SizedBox(height: paddingSize),
                  TextFormField(
                    controller: loginController.cpfController,
                    validator: (value) {
                      return CPFValidator.isValid(value)
                          ? null
                          : 'CPF inválido';
                    },
                    inputFormatters: [cpfFormatter],
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(4),
                      ),
                      hintText: 'CPF',
                      labelText: 'CPF',
                    ),
                  ),
                  const SizedBox(height: paddingSize),
                  ValueListenableBuilder<bool>(
                      valueListenable: loginController.showPassword,
                      builder: (_, showPassword, __) {
                        return TextField(
                          controller: loginController.passwordController,
                          obscureText: !loginController.showPassword.value,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(4),
                            ),
                            hintText: 'Senha',
                            labelText: 'Senha',
                            suffixIcon: IconButton(
                              onPressed: loginController.shouldShowPassword,
                              icon: Icon(
                                showPassword
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                              ),
                            ),
                          ),
                        );
                      }),
                  const SizedBox(height: paddingSize),
                  InkWell(
                    onTap: () async {
                      await loginController.handleLoginSubmit(context);
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      child: Container(
                        color: primaryColor,
                        width: double.infinity,
                        height: 56,
                        child: const Center(
                          child: Text(
                            'ENTRAR',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: paddingSize),
                  TextButton(
                    onPressed: () {
                      Get.to(() => RegisterView());
                    },
                    child: Text(
                      'Cadastrar aqui',
                      style: Theme.of(context).textTheme.button?.copyWith(
                            decoration: TextDecoration.underline,
                          ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
