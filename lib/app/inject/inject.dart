import 'package:get_it/get_it.dart';
import 'package:quirons_user/app/controllers/cipa_controller.dart';
import 'package:quirons_user/app/controllers/exam_controller.dart';
import 'package:quirons_user/app/controllers/ipe_controller.dart';
import 'package:quirons_user/app/controllers/profile_controller.dart';
import 'package:quirons_user/app/controllers/risk_controller.dart';
import 'package:quirons_user/app/interfaces/cipa_repository_interface.dart';
import 'package:quirons_user/app/interfaces/exam_repository_interface.dart';
import 'package:quirons_user/app/interfaces/ipe_repository_interface.dart';
import 'package:quirons_user/app/interfaces/profile_repository_interface.dart';
import 'package:quirons_user/app/interfaces/risk_repository_interface.dart';
import 'package:quirons_user/app/repositories/cipa_repository.dart';
import 'package:quirons_user/app/repositories/exam_repository.dart';
import 'package:quirons_user/app/repositories/ipe_repository.dart';
import 'package:quirons_user/app/repositories/profile_repository.dart';
import 'package:quirons_user/app/repositories/risk_repository.dart';

class Inject {
  static void init() {
    GetIt getIt = GetIt.instance;

    getIt.registerLazySingleton<ICipaRepository>(() => CipaRepository());
    getIt.registerFactory<CipaController>(() => CipaController(getIt()));

    getIt.registerLazySingleton<IIpeRepository>(() => IpeRepository());
    getIt.registerFactory<IpeController>(() => IpeController(getIt()));

    getIt.registerLazySingleton<IExamRepository>(() => ExamRepository());
    getIt.registerFactory<ExamController>(() => ExamController(getIt()));

    getIt.registerLazySingleton<IRiskRepository>(() => RiskRepository());
    getIt.registerFactory<RiskController>(() => RiskController(getIt()));

    getIt.registerLazySingleton<IProfileRepository>(() => ProfileRepository());
    getIt.registerFactory<ProfileController>(() => ProfileController(getIt()));
  }
}
