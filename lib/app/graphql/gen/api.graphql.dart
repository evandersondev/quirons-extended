// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart = 2.12

import 'package:artemis/artemis.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:gql/ast.dart';
import '../../graphql/coercers.dart';
part 'api.graphql.g.dart';

mixin EmployeeCostCenterFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String description;
}
mixin EmployeeDepartmentFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String description;
}
mixin EmployeeOccupationFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String description;
}
mixin EmployeeWorkShiftFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String description;
}
mixin EmployeePositionFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String description;
}
mixin EmployeeExamFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String name;
}
mixin EmployeeCandidateFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String id;
  late EmployeeCandidateFragmentMixin$Person person;
}
mixin EmployeePersonFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String id;
  late String name;
  String? registration;
  String? suggestion;
  String? profilePicture;
}
mixin EmployeeRiskIpeFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String description;
  late double change;
}
mixin EmployeeControlMeasureFragmentMixin {
  @JsonKey(name: '__typename')
  String? $$typename;
  late String description;
}

@JsonSerializable(explicitToJson: true)
class EmployeeVote$Mutation$EmployeeVote extends JsonSerializable
    with EquatableMixin {
  EmployeeVote$Mutation$EmployeeVote();

  factory EmployeeVote$Mutation$EmployeeVote.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeVote$Mutation$EmployeeVoteFromJson(json);

  late String id;

  late String mandateId;

  @override
  List<Object?> get props => [id, mandateId];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeVote$Mutation$EmployeeVoteToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeVote$Mutation extends JsonSerializable with EquatableMixin {
  EmployeeVote$Mutation();

  factory EmployeeVote$Mutation.fromJson(Map<String, dynamic> json) =>
      _$EmployeeVote$MutationFromJson(json);

  late EmployeeVote$Mutation$EmployeeVote employeeVote;

  @override
  List<Object?> get props => [employeeVote];
  @override
  Map<String, dynamic> toJson() => _$EmployeeVote$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeVoteInput extends JsonSerializable with EquatableMixin {
  EmployeeVoteInput(
      {required this.organizationId,
      required this.individualRegistration,
      required this.password,
      required this.mandateId,
      this.candidateId,
      this.nullVote,
      this.whiteVote});

  factory EmployeeVoteInput.fromJson(Map<String, dynamic> json) =>
      _$EmployeeVoteInputFromJson(json);

  late String organizationId;

  late String individualRegistration;

  late String password;

  late String mandateId;

  String? candidateId;

  bool? nullVote;

  bool? whiteVote;

  @override
  List<Object?> get props => [
        organizationId,
        individualRegistration,
        password,
        mandateId,
        candidateId,
        nullVote,
        whiteVote
      ];
  @override
  Map<String, dynamic> toJson() => _$EmployeeVoteInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeCandidate$Mutation$EmployeeCandidate extends JsonSerializable
    with EquatableMixin {
  EmployeeCandidate$Mutation$EmployeeCandidate();

  factory EmployeeCandidate$Mutation$EmployeeCandidate.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeCandidate$Mutation$EmployeeCandidateFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeCandidate$Mutation$EmployeeCandidateToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeCandidate$Mutation extends JsonSerializable with EquatableMixin {
  EmployeeCandidate$Mutation();

  factory EmployeeCandidate$Mutation.fromJson(Map<String, dynamic> json) =>
      _$EmployeeCandidate$MutationFromJson(json);

  late EmployeeCandidate$Mutation$EmployeeCandidate employeeCandidate;

  @override
  List<Object?> get props => [employeeCandidate];
  @override
  Map<String, dynamic> toJson() => _$EmployeeCandidate$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeCandidateInput extends JsonSerializable with EquatableMixin {
  EmployeeCandidateInput(
      {required this.organizationId,
      required this.individualRegistration,
      required this.password,
      required this.mandateId});

  factory EmployeeCandidateInput.fromJson(Map<String, dynamic> json) =>
      _$EmployeeCandidateInputFromJson(json);

  late String organizationId;

  late String individualRegistration;

  late String password;

  late String mandateId;

  @override
  List<Object?> get props =>
      [organizationId, individualRegistration, password, mandateId];
  @override
  Map<String, dynamic> toJson() => _$EmployeeCandidateInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GeneratePassword$Mutation$GeneratePassword extends JsonSerializable
    with EquatableMixin {
  GeneratePassword$Mutation$GeneratePassword();

  factory GeneratePassword$Mutation$GeneratePassword.fromJson(
          Map<String, dynamic> json) =>
      _$GeneratePassword$Mutation$GeneratePasswordFromJson(json);

  String? email;

  String? user;

  String? socialName;

  @override
  List<Object?> get props => [email, user, socialName];
  @override
  Map<String, dynamic> toJson() =>
      _$GeneratePassword$Mutation$GeneratePasswordToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GeneratePassword$Mutation extends JsonSerializable with EquatableMixin {
  GeneratePassword$Mutation();

  factory GeneratePassword$Mutation.fromJson(Map<String, dynamic> json) =>
      _$GeneratePassword$MutationFromJson(json);

  late GeneratePassword$Mutation$GeneratePassword generatePassword;

  @override
  List<Object?> get props => [generatePassword];
  @override
  Map<String, dynamic> toJson() => _$GeneratePassword$MutationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class GeneratePasswordInput extends JsonSerializable with EquatableMixin {
  GeneratePasswordInput(
      {required this.organizationId,
      required this.individualRegistration,
      required this.password,
      required this.registration,
      required this.birthday,
      required this.operation});

  factory GeneratePasswordInput.fromJson(Map<String, dynamic> json) =>
      _$GeneratePasswordInputFromJson(json);

  late String organizationId;

  late String individualRegistration;

  late String password;

  late String registration;

  @JsonKey(
      fromJson: fromGraphQLDateTimeToDartDateTime,
      toJson: fromDartDateTimeToGraphQLDateTime)
  late DateTime birthday;

  @JsonKey(unknownEnumValue: EmployeeOperationType.artemisUnknown)
  late EmployeeOperationType operation;

  @override
  List<Object?> get props => [
        organizationId,
        individualRegistration,
        password,
        registration,
        birthday,
        operation
      ];
  @override
  Map<String, dynamic> toJson() => _$GeneratePasswordInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeIpes$Query$EmployeeIpes extends JsonSerializable
    with EquatableMixin {
  EmployeeIpes$Query$EmployeeIpes();

  factory EmployeeIpes$Query$EmployeeIpes.fromJson(Map<String, dynamic> json) =>
      _$EmployeeIpes$Query$EmployeeIpesFromJson(json);

  late String equipmentId;

  late String equipmentDescription;

  @JsonKey(
      fromJson: fromGraphQLDateTimeNullableToDartDateTimeNullable,
      toJson: fromDartDateTimeNullableToGraphQLDateTimeNullable)
  DateTime? nextChange;

  String? caCode;

  @JsonKey(
      fromJson: fromGraphQLDateTimeNullableToDartDateTimeNullable,
      toJson: fromDartDateTimeNullableToGraphQLDateTimeNullable)
  DateTime? date;

  @JsonKey(
      fromJson: fromGraphQLDateTimeNullableToDartDateTimeNullable,
      toJson: fromDartDateTimeNullableToGraphQLDateTimeNullable)
  DateTime? lastMaintenance;

  @JsonKey(unknownEnumValue: DeliveryReason.artemisUnknown)
  DeliveryReason? reason;

  double? amount;

  bool? effective;

  String? observation;

  @override
  List<Object?> get props => [
        equipmentId,
        equipmentDescription,
        nextChange,
        caCode,
        date,
        lastMaintenance,
        reason,
        amount,
        effective,
        observation
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeIpes$Query$EmployeeIpesToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeIpes$Query extends JsonSerializable with EquatableMixin {
  EmployeeIpes$Query();

  factory EmployeeIpes$Query.fromJson(Map<String, dynamic> json) =>
      _$EmployeeIpes$QueryFromJson(json);

  late List<EmployeeIpes$Query$EmployeeIpes> employeeIpes;

  @override
  List<Object?> get props => [employeeIpes];
  @override
  Map<String, dynamic> toJson() => _$EmployeeIpes$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class BaseInput extends JsonSerializable with EquatableMixin {
  BaseInput(
      {required this.organizationId,
      required this.individualRegistration,
      required this.password});

  factory BaseInput.fromJson(Map<String, dynamic> json) =>
      _$BaseInputFromJson(json);

  late String organizationId;

  late String individualRegistration;

  late String password;

  @override
  List<Object?> get props => [organizationId, individualRegistration, password];
  @override
  Map<String, dynamic> toJson() => _$BaseInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeHasVote$Query$EmployeeHasVote extends JsonSerializable
    with EquatableMixin {
  EmployeeHasVote$Query$EmployeeHasVote();

  factory EmployeeHasVote$Query$EmployeeHasVote.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeHasVote$Query$EmployeeHasVoteFromJson(json);

  late String id;

  late String mandateId;

  late String cypherUser;

  bool? nullVote;

  bool? whiteVote;

  String? candidateId;

  @override
  List<Object?> get props =>
      [id, mandateId, cypherUser, nullVote, whiteVote, candidateId];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeHasVote$Query$EmployeeHasVoteToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeHasVote$Query extends JsonSerializable with EquatableMixin {
  EmployeeHasVote$Query();

  factory EmployeeHasVote$Query.fromJson(Map<String, dynamic> json) =>
      _$EmployeeHasVote$QueryFromJson(json);

  EmployeeHasVote$Query$EmployeeHasVote? employeeHasVote;

  @override
  List<Object?> get props => [employeeHasVote];
  @override
  Map<String, dynamic> toJson() => _$EmployeeHasVote$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeHasVoteInput extends JsonSerializable with EquatableMixin {
  EmployeeHasVoteInput(
      {required this.organizationId,
      required this.individualRegistration,
      required this.password,
      required this.mandateId});

  factory EmployeeHasVoteInput.fromJson(Map<String, dynamic> json) =>
      _$EmployeeHasVoteInputFromJson(json);

  late String organizationId;

  late String individualRegistration;

  late String password;

  late String mandateId;

  @override
  List<Object?> get props =>
      [organizationId, individualRegistration, password, mandateId];
  @override
  Map<String, dynamic> toJson() => _$EmployeeHasVoteInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeInfo$Query$EmployeeInfo$CostCenter extends JsonSerializable
    with EquatableMixin, EmployeeCostCenterFragmentMixin {
  EmployeeInfo$Query$EmployeeInfo$CostCenter();

  factory EmployeeInfo$Query$EmployeeInfo$CostCenter.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeInfo$Query$EmployeeInfo$CostCenterFromJson(json);

  @override
  List<Object?> get props => [$$typename, description];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeInfo$Query$EmployeeInfo$CostCenterToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeInfo$Query$EmployeeInfo$Department extends JsonSerializable
    with EquatableMixin, EmployeeDepartmentFragmentMixin {
  EmployeeInfo$Query$EmployeeInfo$Department();

  factory EmployeeInfo$Query$EmployeeInfo$Department.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeInfo$Query$EmployeeInfo$DepartmentFromJson(json);

  @override
  List<Object?> get props => [$$typename, description];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeInfo$Query$EmployeeInfo$DepartmentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeInfo$Query$EmployeeInfo$Occupation extends JsonSerializable
    with EquatableMixin, EmployeeOccupationFragmentMixin {
  EmployeeInfo$Query$EmployeeInfo$Occupation();

  factory EmployeeInfo$Query$EmployeeInfo$Occupation.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeInfo$Query$EmployeeInfo$OccupationFromJson(json);

  @override
  List<Object?> get props => [$$typename, description];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeInfo$Query$EmployeeInfo$OccupationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeInfo$Query$EmployeeInfo$WorkShift extends JsonSerializable
    with EquatableMixin, EmployeeWorkShiftFragmentMixin {
  EmployeeInfo$Query$EmployeeInfo$WorkShift();

  factory EmployeeInfo$Query$EmployeeInfo$WorkShift.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeInfo$Query$EmployeeInfo$WorkShiftFromJson(json);

  @override
  List<Object?> get props => [$$typename, description];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeInfo$Query$EmployeeInfo$WorkShiftToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeInfo$Query$EmployeeInfo$Position extends JsonSerializable
    with EquatableMixin, EmployeePositionFragmentMixin {
  EmployeeInfo$Query$EmployeeInfo$Position();

  factory EmployeeInfo$Query$EmployeeInfo$Position.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeInfo$Query$EmployeeInfo$PositionFromJson(json);

  @override
  List<Object?> get props => [$$typename, description];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeInfo$Query$EmployeeInfo$PositionToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeInfo$Query$EmployeeInfo extends JsonSerializable
    with EquatableMixin {
  EmployeeInfo$Query$EmployeeInfo();

  factory EmployeeInfo$Query$EmployeeInfo.fromJson(Map<String, dynamic> json) =>
      _$EmployeeInfo$Query$EmployeeInfoFromJson(json);

  String? profilePicture;

  late String name;

  String? email;

  @JsonKey(unknownEnumValue: Gender.artemisUnknown)
  late Gender gender;

  String? address;

  late String individualRegistration;

  @JsonKey(unknownEnumValue: EducationLevel.artemisUnknown)
  EducationLevel? educationLevel;

  String? registration;

  EmployeeInfo$Query$EmployeeInfo$CostCenter? costCenter;

  EmployeeInfo$Query$EmployeeInfo$Department? department;

  EmployeeInfo$Query$EmployeeInfo$Occupation? occupation;

  EmployeeInfo$Query$EmployeeInfo$WorkShift? workShift;

  EmployeeInfo$Query$EmployeeInfo$Position? position;

  @override
  List<Object?> get props => [
        profilePicture,
        name,
        email,
        gender,
        address,
        individualRegistration,
        educationLevel,
        registration,
        costCenter,
        department,
        occupation,
        workShift,
        position
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeInfo$Query$EmployeeInfoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeInfo$Query extends JsonSerializable with EquatableMixin {
  EmployeeInfo$Query();

  factory EmployeeInfo$Query.fromJson(Map<String, dynamic> json) =>
      _$EmployeeInfo$QueryFromJson(json);

  EmployeeInfo$Query$EmployeeInfo? employeeInfo;

  @override
  List<Object?> get props => [employeeInfo];
  @override
  Map<String, dynamic> toJson() => _$EmployeeInfo$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeConsultations$Query$EmployeeConsultations$Exams
    extends JsonSerializable with EquatableMixin, EmployeeExamFragmentMixin {
  EmployeeConsultations$Query$EmployeeConsultations$Exams();

  factory EmployeeConsultations$Query$EmployeeConsultations$Exams.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeConsultations$Query$EmployeeConsultations$ExamsFromJson(json);

  @override
  List<Object?> get props => [$$typename, name];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeConsultations$Query$EmployeeConsultations$ExamsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeConsultations$Query$EmployeeConsultations extends JsonSerializable
    with EquatableMixin {
  EmployeeConsultations$Query$EmployeeConsultations();

  factory EmployeeConsultations$Query$EmployeeConsultations.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeConsultations$Query$EmployeeConsultationsFromJson(json);

  late String doctorName;

  late String reason;

  @JsonKey(unknownEnumValue: ConsultationType.artemisUnknown)
  late ConsultationType type;

  @JsonKey(
      fromJson: fromGraphQLDateTimeToDartDateTime,
      toJson: fromDartDateTimeToGraphQLDateTime)
  late DateTime estimatedDate;

  @JsonKey(unknownEnumValue: ConsultationNature.artemisUnknown)
  ConsultationNature? nature;

  double? estimatedTime;

  @JsonKey(
      fromJson: fromGraphQLDateTimeNullableToDartDateTimeNullable,
      toJson: fromDartDateTimeNullableToGraphQLDateTimeNullable)
  DateTime? startDate;

  @JsonKey(
      fromJson: fromGraphQLDateTimeNullableToDartDateTimeNullable,
      toJson: fromDartDateTimeNullableToGraphQLDateTimeNullable)
  DateTime? endDate;

  String? accreditedName;

  late List<EmployeeConsultations$Query$EmployeeConsultations$Exams> exams;

  @override
  List<Object?> get props => [
        doctorName,
        reason,
        type,
        estimatedDate,
        nature,
        estimatedTime,
        startDate,
        endDate,
        accreditedName,
        exams
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeConsultations$Query$EmployeeConsultationsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeConsultations$Query extends JsonSerializable with EquatableMixin {
  EmployeeConsultations$Query();

  factory EmployeeConsultations$Query.fromJson(Map<String, dynamic> json) =>
      _$EmployeeConsultations$QueryFromJson(json);

  late List<EmployeeConsultations$Query$EmployeeConsultations>
      employeeConsultations;

  @override
  List<Object?> get props => [employeeConsultations];
  @override
  Map<String, dynamic> toJson() => _$EmployeeConsultations$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeMandates$Query$EmployeeMandates$Candidates
    extends JsonSerializable
    with EquatableMixin, EmployeeCandidateFragmentMixin {
  EmployeeMandates$Query$EmployeeMandates$Candidates();

  factory EmployeeMandates$Query$EmployeeMandates$Candidates.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeMandates$Query$EmployeeMandates$CandidatesFromJson(json);

  @override
  List<Object?> get props => [$$typename, id, person];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeMandates$Query$EmployeeMandates$CandidatesToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeMandates$Query$EmployeeMandates extends JsonSerializable
    with EquatableMixin {
  EmployeeMandates$Query$EmployeeMandates();

  factory EmployeeMandates$Query$EmployeeMandates.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeMandates$Query$EmployeeMandatesFromJson(json);

  late String mandateId;

  late String description;

  List<EmployeeMandates$Query$EmployeeMandates$Candidates>? candidates;

  @override
  List<Object?> get props => [mandateId, description, candidates];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeMandates$Query$EmployeeMandatesToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeMandates$Query extends JsonSerializable with EquatableMixin {
  EmployeeMandates$Query();

  factory EmployeeMandates$Query.fromJson(Map<String, dynamic> json) =>
      _$EmployeeMandates$QueryFromJson(json);

  late List<EmployeeMandates$Query$EmployeeMandates> employeeMandates;

  @override
  List<Object?> get props => [employeeMandates];
  @override
  Map<String, dynamic> toJson() => _$EmployeeMandates$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeCandidateFragmentMixin$Person extends JsonSerializable
    with EquatableMixin, EmployeePersonFragmentMixin {
  EmployeeCandidateFragmentMixin$Person();

  factory EmployeeCandidateFragmentMixin$Person.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeCandidateFragmentMixin$PersonFromJson(json);

  @override
  List<Object?> get props =>
      [$$typename, id, name, registration, suggestion, profilePicture];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeCandidateFragmentMixin$PersonToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeMandateInput extends JsonSerializable with EquatableMixin {
  EmployeeMandateInput(
      {required this.organizationId,
      required this.individualRegistration,
      required this.password,
      required this.type});

  factory EmployeeMandateInput.fromJson(Map<String, dynamic> json) =>
      _$EmployeeMandateInputFromJson(json);

  late String organizationId;

  late String individualRegistration;

  late String password;

  @JsonKey(unknownEnumValue: MandateEventType.artemisUnknown)
  late MandateEventType type;

  @override
  List<Object?> get props =>
      [organizationId, individualRegistration, password, type];
  @override
  Map<String, dynamic> toJson() => _$EmployeeMandateInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeLogin$Query$EmployeeLogin$Department extends JsonSerializable
    with EquatableMixin {
  EmployeeLogin$Query$EmployeeLogin$Department();

  factory EmployeeLogin$Query$EmployeeLogin$Department.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeLogin$Query$EmployeeLogin$DepartmentFromJson(json);

  late String description;

  @override
  List<Object?> get props => [description];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeLogin$Query$EmployeeLogin$DepartmentToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeLogin$Query$EmployeeLogin$Occupation extends JsonSerializable
    with EquatableMixin {
  EmployeeLogin$Query$EmployeeLogin$Occupation();

  factory EmployeeLogin$Query$EmployeeLogin$Occupation.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeLogin$Query$EmployeeLogin$OccupationFromJson(json);

  late String description;

  @override
  List<Object?> get props => [description];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeLogin$Query$EmployeeLogin$OccupationToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeLogin$Query$EmployeeLogin extends JsonSerializable
    with EquatableMixin {
  EmployeeLogin$Query$EmployeeLogin();

  factory EmployeeLogin$Query$EmployeeLogin.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeLogin$Query$EmployeeLoginFromJson(json);

  late String id;

  late bool isActive;

  String? socialName;

  String? generalRegistration;

  String? email;

  String? registration;

  String? user;

  String? profilePicture;

  String? uploadUrl;

  @JsonKey(
      fromJson: fromGraphQLDateTimeNullableToDartDateTimeNullable,
      toJson: fromDartDateTimeNullableToGraphQLDateTimeNullable)
  DateTime? admissionDate;

  String? pisCode;

  late String name;

  @JsonKey(unknownEnumValue: EducationLevel.artemisUnknown)
  EducationLevel? educationLevel;

  late String individualRegistration;

  @JsonKey(
      fromJson: fromGraphQLDateTimeToDartDateTime,
      toJson: fromDartDateTimeToGraphQLDateTime)
  late DateTime birthday;

  @JsonKey(unknownEnumValue: Gender.artemisUnknown)
  late Gender gender;

  EmployeeLogin$Query$EmployeeLogin$Department? department;

  EmployeeLogin$Query$EmployeeLogin$Occupation? occupation;

  @override
  List<Object?> get props => [
        id,
        isActive,
        socialName,
        generalRegistration,
        email,
        registration,
        user,
        profilePicture,
        uploadUrl,
        admissionDate,
        pisCode,
        name,
        educationLevel,
        individualRegistration,
        birthday,
        gender,
        department,
        occupation
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeLogin$Query$EmployeeLoginToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeLogin$Query extends JsonSerializable with EquatableMixin {
  EmployeeLogin$Query();

  factory EmployeeLogin$Query.fromJson(Map<String, dynamic> json) =>
      _$EmployeeLogin$QueryFromJson(json);

  late EmployeeLogin$Query$EmployeeLogin employeeLogin;

  @override
  List<Object?> get props => [employeeLogin];
  @override
  Map<String, dynamic> toJson() => _$EmployeeLogin$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeHasCandidate$Query$EmployeeHasCandidate extends JsonSerializable
    with EquatableMixin {
  EmployeeHasCandidate$Query$EmployeeHasCandidate();

  factory EmployeeHasCandidate$Query$EmployeeHasCandidate.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeHasCandidate$Query$EmployeeHasCandidateFromJson(json);

  late String id;

  @override
  List<Object?> get props => [id];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeHasCandidate$Query$EmployeeHasCandidateToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeHasCandidate$Query extends JsonSerializable with EquatableMixin {
  EmployeeHasCandidate$Query();

  factory EmployeeHasCandidate$Query.fromJson(Map<String, dynamic> json) =>
      _$EmployeeHasCandidate$QueryFromJson(json);

  EmployeeHasCandidate$Query$EmployeeHasCandidate? employeeHasCandidate;

  @override
  List<Object?> get props => [employeeHasCandidate];
  @override
  Map<String, dynamic> toJson() => _$EmployeeHasCandidate$QueryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeHasCandidateInput extends JsonSerializable with EquatableMixin {
  EmployeeHasCandidateInput(
      {required this.organizationId,
      required this.individualRegistration,
      required this.password,
      required this.mandateId});

  factory EmployeeHasCandidateInput.fromJson(Map<String, dynamic> json) =>
      _$EmployeeHasCandidateInputFromJson(json);

  late String organizationId;

  late String individualRegistration;

  late String password;

  late String mandateId;

  @override
  List<Object?> get props =>
      [organizationId, individualRegistration, password, mandateId];
  @override
  Map<String, dynamic> toJson() => _$EmployeeHasCandidateInputToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeRisks$Query$EmployeeRisks$Ipes extends JsonSerializable
    with EquatableMixin, EmployeeRiskIpeFragmentMixin {
  EmployeeRisks$Query$EmployeeRisks$Ipes();

  factory EmployeeRisks$Query$EmployeeRisks$Ipes.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeRisks$Query$EmployeeRisks$IpesFromJson(json);

  @override
  List<Object?> get props => [$$typename, description, change];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeRisks$Query$EmployeeRisks$IpesToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeRisks$Query$EmployeeRisks$ControlMeasures extends JsonSerializable
    with EquatableMixin, EmployeeControlMeasureFragmentMixin {
  EmployeeRisks$Query$EmployeeRisks$ControlMeasures();

  factory EmployeeRisks$Query$EmployeeRisks$ControlMeasures.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeRisks$Query$EmployeeRisks$ControlMeasuresFromJson(json);

  @override
  List<Object?> get props => [$$typename, description];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeRisks$Query$EmployeeRisks$ControlMeasuresToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeRisks$Query$EmployeeRisks extends JsonSerializable
    with EquatableMixin {
  EmployeeRisks$Query$EmployeeRisks();

  factory EmployeeRisks$Query$EmployeeRisks.fromJson(
          Map<String, dynamic> json) =>
      _$EmployeeRisks$Query$EmployeeRisksFromJson(json);

  @JsonKey(
      fromJson: fromGraphQLDateTimeToDartDateTime,
      toJson: fromDartDateTimeToGraphQLDateTime)
  late DateTime recognitionDate;

  late String riskAgent;

  @JsonKey(unknownEnumValue: AgentGroup.artemisUnknown)
  late AgentGroup riskGroup;

  late String generationSource;

  late String physicalEnvironment;

  @JsonKey(unknownEnumValue: RiskMeasureType.artemisUnknown)
  RiskMeasureType? measureType;

  @JsonKey(unknownEnumValue: RiskMeasure.artemisUnknown)
  RiskMeasure? unitMeasurement;

  double? actionLevel;

  String? exposureTime;

  String? repetition;

  String? interval;

  @JsonKey(unknownEnumValue: RiskDegree.artemisUnknown)
  RiskDegree? riskDegree;

  @JsonKey(unknownEnumValue: RiskMap.artemisUnknown)
  RiskMap? riskMap;

  @JsonKey(unknownEnumValue: RiskCategory.artemisUnknown)
  RiskCategory? category;

  @JsonKey(unknownEnumValue: RiskProbability.artemisUnknown)
  RiskProbability? probability;

  @JsonKey(unknownEnumValue: RiskSeverity.artemisUnknown)
  RiskSeverity? severity;

  late bool protectionMeasure;

  late bool workingCondition;

  late bool uninterruptedUse;

  late bool expirationDate;

  late bool exchangePeriodicity;

  late bool sanitation;

  bool? needIpe;

  bool? needCpe;

  late List<EmployeeRisks$Query$EmployeeRisks$Ipes> ipes;

  late List<EmployeeRisks$Query$EmployeeRisks$ControlMeasures> controlMeasures;

  @override
  List<Object?> get props => [
        recognitionDate,
        riskAgent,
        riskGroup,
        generationSource,
        physicalEnvironment,
        measureType,
        unitMeasurement,
        actionLevel,
        exposureTime,
        repetition,
        interval,
        riskDegree,
        riskMap,
        category,
        probability,
        severity,
        protectionMeasure,
        workingCondition,
        uninterruptedUse,
        expirationDate,
        exchangePeriodicity,
        sanitation,
        needIpe,
        needCpe,
        ipes,
        controlMeasures
      ];
  @override
  Map<String, dynamic> toJson() =>
      _$EmployeeRisks$Query$EmployeeRisksToJson(this);
}

@JsonSerializable(explicitToJson: true)
class EmployeeRisks$Query extends JsonSerializable with EquatableMixin {
  EmployeeRisks$Query();

  factory EmployeeRisks$Query.fromJson(Map<String, dynamic> json) =>
      _$EmployeeRisks$QueryFromJson(json);

  late List<EmployeeRisks$Query$EmployeeRisks> employeeRisks;

  @override
  List<Object?> get props => [employeeRisks];
  @override
  Map<String, dynamic> toJson() => _$EmployeeRisks$QueryToJson(this);
}

enum EmployeeOperationType {
  @JsonValue('Create')
  create,
  @JsonValue('Change')
  change,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum DeliveryReason {
  @JsonValue('Admissional')
  admissional,
  @JsonValue('Wear')
  wear,
  @JsonValue('Defect')
  defect,
  @JsonValue('Loss')
  loss,
  @JsonValue('Theft')
  theft,
  @JsonValue('Demissional')
  demissional,
  @JsonValue('Sanitation')
  sanitation,
  @JsonValue('FunctionChange')
  functionChange,
  @JsonValue('DielectricTest')
  dielectricTest,
  @JsonValue('Other')
  other,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum Gender {
  @JsonValue('Male')
  male,
  @JsonValue('Female')
  female,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum EducationLevel {
  @JsonValue('ElementarySchool')
  elementarySchool,
  @JsonValue('HighSchool')
  highSchool,
  @JsonValue('UniversityGraduate')
  universityGraduate,
  @JsonValue('MastersDegree')
  mastersDegree,
  @JsonValue('DoctorateDegree')
  doctorateDegree,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum ConsultationType {
  @JsonValue('Occupational')
  occupational,
  @JsonValue('Clinical')
  clinical,
  @JsonValue('Urgency')
  urgency,
  @JsonValue('QualityOfLife')
  qualityOfLife,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum ConsultationNature {
  @JsonValue('Admissional')
  admissional,
  @JsonValue('Periodic')
  periodic,
  @JsonValue('BackToWork')
  backToWork,
  @JsonValue('ChangeOfFunction')
  changeOfFunction,
  @JsonValue('Dismissal')
  dismissal,
  @JsonValue('SpotMonitoring')
  spotMonitoring,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum MandateEventType {
  @JsonValue('Candidate')
  candidate,
  @JsonValue('Vote')
  vote,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum AgentGroup {
  @JsonValue('Physical')
  physical,
  @JsonValue('Chemical')
  chemical,
  @JsonValue('Biological')
  biological,
  @JsonValue('Ergonomic')
  ergonomic,
  @JsonValue('Mechanical')
  mechanical,
  @JsonValue('Dangerous')
  dangerous,
  @JsonValue('Association')
  association,
  @JsonValue('Other')
  other,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum RiskMeasureType {
  @JsonValue('above')
  above,
  @JsonValue('aboveOrEqual')
  aboveOrEqual,
  @JsonValue('equal')
  equal,
  @JsonValue('belowOrEqual')
  belowOrEqual,
  @JsonValue('below')
  below,
  @JsonValue('na')
  na,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum RiskMeasure {
  @JsonValue('NumeroAdimensional')
  numeroAdimensional,
  @JsonValue('dBLinear')
  dBLinear,
  @JsonValue('dBC')
  dBC,
  @JsonValue('dBA')
  dBA,
  @JsonValue('kCalH')
  kCalH,
  @JsonValue('gy')
  gy,
  @JsonValue('sv')
  sv,
  @JsonValue('kgfCm2')
  kgfCm2,
  @JsonValue('mS2')
  mS2,
  @JsonValue('mS175')
  mS175,
  @JsonValue('ppm')
  ppm,
  @JsonValue('mgM3')
  mgM3,
  @JsonValue('fCm3')
  fCm3,
  @JsonValue('oC')
  oC,
  @JsonValue('mS')
  mS,
  @JsonValue('percentual')
  percentual,
  @JsonValue('lx')
  lx,
  @JsonValue('ufcM3')
  ufcM3,
  @JsonValue('doseDiaria')
  doseDiaria,
  @JsonValue('doseMensal')
  doseMensal,
  @JsonValue('doseTrimestral')
  doseTrimestral,
  @JsonValue('doseAnual')
  doseAnual,
  @JsonValue('hz')
  hz,
  @JsonValue('ghz')
  ghz,
  @JsonValue('kHz')
  kHz,
  @JsonValue('wM2')
  wM2,
  @JsonValue('aM')
  aM,
  @JsonValue('mT')
  mT,
  @JsonValue('emiT')
  emiT,
  @JsonValue('mA')
  mA,
  @JsonValue('kvM')
  kvM,
  @JsonValue('vM')
  vM,
  @JsonValue('min')
  min,
  @JsonValue('h')
  h,
  @JsonValue('jM2')
  jM2,
  @JsonValue('mjCm2')
  mjCm2,
  @JsonValue('mSv')
  mSv,
  @JsonValue('atm')
  atm,
  @JsonValue('mppdc')
  mppdc,
  @JsonValue('nm')
  nm,
  @JsonValue('mW')
  mW,
  @JsonValue('w')
  w,
  @JsonValue('ur')
  ur,
  @JsonValue('nA')
  nA,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum RiskDegree {
  @JsonValue('Small')
  small,
  @JsonValue('Medium')
  medium,
  @JsonValue('Large')
  large,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum RiskMap {
  @JsonValue('CIPA')
  cipa,
  @JsonValue('SESMT')
  sesmt,
  @JsonValue('Both')
  both,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum RiskCategory {
  @JsonValue('Irrelevant')
  irrelevant,
  @JsonValue('Attention')
  attention,
  @JsonValue('Critical')
  critical,
  @JsonValue('Intolerable')
  intolerable,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum RiskProbability {
  @JsonValue('Low')
  low,
  @JsonValue('Intermediate')
  intermediate,
  @JsonValue('Medium')
  medium,
  @JsonValue('High')
  high,
  @JsonValue('VeryHigh')
  veryHigh,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

enum RiskSeverity {
  @JsonValue('Mild')
  mild,
  @JsonValue('Intermediate')
  intermediate,
  @JsonValue('Moderate')
  moderate,
  @JsonValue('Severe')
  severe,
  @JsonValue('VerySevere')
  verySevere,
  @JsonValue('ARTEMIS_UNKNOWN')
  artemisUnknown,
}

@JsonSerializable(explicitToJson: true)
class EmployeeVoteArguments extends JsonSerializable with EquatableMixin {
  EmployeeVoteArguments({required this.vote});

  @override
  factory EmployeeVoteArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeVoteArgumentsFromJson(json);

  late EmployeeVoteInput vote;

  @override
  List<Object?> get props => [vote];
  @override
  Map<String, dynamic> toJson() => _$EmployeeVoteArgumentsToJson(this);
}

final EMPLOYEE_VOTE_MUTATION_DOCUMENT_OPERATION_NAME = 'employeeVote';
final EMPLOYEE_VOTE_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'employeeVote'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'vote')),
            type: NamedTypeNode(
                name: NameNode(value: 'EmployeeVoteInput'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeVote'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'vote'),
                  value: VariableNode(name: NameNode(value: 'vote')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'mandateId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class EmployeeVoteMutation
    extends GraphQLQuery<EmployeeVote$Mutation, EmployeeVoteArguments> {
  EmployeeVoteMutation({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_VOTE_MUTATION_DOCUMENT;

  @override
  final String operationName = EMPLOYEE_VOTE_MUTATION_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeVoteArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeVote$Mutation parse(Map<String, dynamic> json) =>
      EmployeeVote$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class EmployeeCandidateArguments extends JsonSerializable with EquatableMixin {
  EmployeeCandidateArguments({required this.candidate});

  @override
  factory EmployeeCandidateArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeCandidateArgumentsFromJson(json);

  late EmployeeCandidateInput candidate;

  @override
  List<Object?> get props => [candidate];
  @override
  Map<String, dynamic> toJson() => _$EmployeeCandidateArgumentsToJson(this);
}

final EMPLOYEE_CANDIDATE_MUTATION_DOCUMENT_OPERATION_NAME = 'employeeCandidate';
final EMPLOYEE_CANDIDATE_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'employeeCandidate'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'candidate')),
            type: NamedTypeNode(
                name: NameNode(value: 'EmployeeCandidateInput'),
                isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeCandidate'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'candidate'),
                  value: VariableNode(name: NameNode(value: 'candidate')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class EmployeeCandidateMutation extends GraphQLQuery<EmployeeCandidate$Mutation,
    EmployeeCandidateArguments> {
  EmployeeCandidateMutation({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_CANDIDATE_MUTATION_DOCUMENT;

  @override
  final String operationName =
      EMPLOYEE_CANDIDATE_MUTATION_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeCandidateArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeCandidate$Mutation parse(Map<String, dynamic> json) =>
      EmployeeCandidate$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class GeneratePasswordArguments extends JsonSerializable with EquatableMixin {
  GeneratePasswordArguments({required this.generate});

  @override
  factory GeneratePasswordArguments.fromJson(Map<String, dynamic> json) =>
      _$GeneratePasswordArgumentsFromJson(json);

  late GeneratePasswordInput generate;

  @override
  List<Object?> get props => [generate];
  @override
  Map<String, dynamic> toJson() => _$GeneratePasswordArgumentsToJson(this);
}

final GENERATE_PASSWORD_MUTATION_DOCUMENT_OPERATION_NAME = 'generatePassword';
final GENERATE_PASSWORD_MUTATION_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.mutation,
      name: NameNode(value: 'generatePassword'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'generate')),
            type: NamedTypeNode(
                name: NameNode(value: 'GeneratePasswordInput'),
                isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'generatePassword'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'generate'),
                  value: VariableNode(name: NameNode(value: 'generate')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'email'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'socialName'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class GeneratePasswordMutation
    extends GraphQLQuery<GeneratePassword$Mutation, GeneratePasswordArguments> {
  GeneratePasswordMutation({required this.variables});

  @override
  final DocumentNode document = GENERATE_PASSWORD_MUTATION_DOCUMENT;

  @override
  final String operationName =
      GENERATE_PASSWORD_MUTATION_DOCUMENT_OPERATION_NAME;

  @override
  final GeneratePasswordArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  GeneratePassword$Mutation parse(Map<String, dynamic> json) =>
      GeneratePassword$Mutation.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class EmployeeIpesArguments extends JsonSerializable with EquatableMixin {
  EmployeeIpesArguments({required this.employee});

  @override
  factory EmployeeIpesArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeIpesArgumentsFromJson(json);

  late BaseInput employee;

  @override
  List<Object?> get props => [employee];
  @override
  Map<String, dynamic> toJson() => _$EmployeeIpesArgumentsToJson(this);
}

final EMPLOYEE_IPES_QUERY_DOCUMENT_OPERATION_NAME = 'employeeIpes';
final EMPLOYEE_IPES_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'employeeIpes'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'employee')),
            type: NamedTypeNode(
                name: NameNode(value: 'BaseInput'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeIpes'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'employee'),
                  value: VariableNode(name: NameNode(value: 'employee')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'equipmentId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'equipmentDescription'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'nextChange'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'caCode'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'date'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'lastMaintenance'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'reason'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'amount'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'effective'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'observation'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class EmployeeIpesQuery
    extends GraphQLQuery<EmployeeIpes$Query, EmployeeIpesArguments> {
  EmployeeIpesQuery({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_IPES_QUERY_DOCUMENT;

  @override
  final String operationName = EMPLOYEE_IPES_QUERY_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeIpesArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeIpes$Query parse(Map<String, dynamic> json) =>
      EmployeeIpes$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class EmployeeHasVoteArguments extends JsonSerializable with EquatableMixin {
  EmployeeHasVoteArguments({required this.employee});

  @override
  factory EmployeeHasVoteArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeHasVoteArgumentsFromJson(json);

  late EmployeeHasVoteInput employee;

  @override
  List<Object?> get props => [employee];
  @override
  Map<String, dynamic> toJson() => _$EmployeeHasVoteArgumentsToJson(this);
}

final EMPLOYEE_HAS_VOTE_QUERY_DOCUMENT_OPERATION_NAME = 'employeeHasVote';
final EMPLOYEE_HAS_VOTE_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'employeeHasVote'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'employee')),
            type: NamedTypeNode(
                name: NameNode(value: 'EmployeeHasVoteInput'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeHasVote'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'employee'),
                  value: VariableNode(name: NameNode(value: 'employee')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'mandateId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'cypherUser'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'nullVote'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'whiteVote'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'candidateId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class EmployeeHasVoteQuery
    extends GraphQLQuery<EmployeeHasVote$Query, EmployeeHasVoteArguments> {
  EmployeeHasVoteQuery({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_HAS_VOTE_QUERY_DOCUMENT;

  @override
  final String operationName = EMPLOYEE_HAS_VOTE_QUERY_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeHasVoteArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeHasVote$Query parse(Map<String, dynamic> json) =>
      EmployeeHasVote$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class EmployeeInfoArguments extends JsonSerializable with EquatableMixin {
  EmployeeInfoArguments({required this.employee});

  @override
  factory EmployeeInfoArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeInfoArgumentsFromJson(json);

  late BaseInput employee;

  @override
  List<Object?> get props => [employee];
  @override
  Map<String, dynamic> toJson() => _$EmployeeInfoArgumentsToJson(this);
}

final EMPLOYEE_INFO_QUERY_DOCUMENT_OPERATION_NAME = 'employeeInfo';
final EMPLOYEE_INFO_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'employeeInfo'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'employee')),
            type: NamedTypeNode(
                name: NameNode(value: 'BaseInput'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeInfo'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'employee'),
                  value: VariableNode(name: NameNode(value: 'employee')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'profilePicture'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'email'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'gender'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'address'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'individualRegistration'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'educationLevel'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'registration'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'costCenter'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FragmentSpreadNode(
                        name: NameNode(value: 'EmployeeCostCenterFragment'),
                        directives: [])
                  ])),
              FieldNode(
                  name: NameNode(value: 'department'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FragmentSpreadNode(
                        name: NameNode(value: 'EmployeeDepartmentFragment'),
                        directives: [])
                  ])),
              FieldNode(
                  name: NameNode(value: 'occupation'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FragmentSpreadNode(
                        name: NameNode(value: 'EmployeeOccupationFragment'),
                        directives: [])
                  ])),
              FieldNode(
                  name: NameNode(value: 'workShift'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FragmentSpreadNode(
                        name: NameNode(value: 'EmployeeWorkShiftFragment'),
                        directives: [])
                  ])),
              FieldNode(
                  name: NameNode(value: 'position'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FragmentSpreadNode(
                        name: NameNode(value: 'EmployeePositionFragment'),
                        directives: [])
                  ]))
            ]))
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeeCostCenterFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'CostCenter'), isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'description'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null)
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeeDepartmentFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'Department'), isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'description'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null)
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeeOccupationFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'Occupation'), isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'description'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null)
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeeWorkShiftFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'WorkShift'), isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'description'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null)
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeePositionFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'Position'), isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'description'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null)
      ]))
]);

class EmployeeInfoQuery
    extends GraphQLQuery<EmployeeInfo$Query, EmployeeInfoArguments> {
  EmployeeInfoQuery({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_INFO_QUERY_DOCUMENT;

  @override
  final String operationName = EMPLOYEE_INFO_QUERY_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeInfoArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeInfo$Query parse(Map<String, dynamic> json) =>
      EmployeeInfo$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class EmployeeConsultationsArguments extends JsonSerializable
    with EquatableMixin {
  EmployeeConsultationsArguments({required this.employee});

  @override
  factory EmployeeConsultationsArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeConsultationsArgumentsFromJson(json);

  late BaseInput employee;

  @override
  List<Object?> get props => [employee];
  @override
  Map<String, dynamic> toJson() => _$EmployeeConsultationsArgumentsToJson(this);
}

final EMPLOYEE_CONSULTATIONS_QUERY_DOCUMENT_OPERATION_NAME =
    'employeeConsultations';
final EMPLOYEE_CONSULTATIONS_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'employeeConsultations'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'employee')),
            type: NamedTypeNode(
                name: NameNode(value: 'BaseInput'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeConsultations'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'employee'),
                  value: VariableNode(name: NameNode(value: 'employee')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'doctorName'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'reason'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'type'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'estimatedDate'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'nature'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'estimatedTime'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'startDate'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'endDate'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'accreditedName'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'exams'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FragmentSpreadNode(
                        name: NameNode(value: 'EmployeeExamFragment'),
                        directives: [])
                  ]))
            ]))
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeeExamFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'EmployeeExam'), isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'name'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null)
      ]))
]);

class EmployeeConsultationsQuery extends GraphQLQuery<
    EmployeeConsultations$Query, EmployeeConsultationsArguments> {
  EmployeeConsultationsQuery({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_CONSULTATIONS_QUERY_DOCUMENT;

  @override
  final String operationName =
      EMPLOYEE_CONSULTATIONS_QUERY_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeConsultationsArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeConsultations$Query parse(Map<String, dynamic> json) =>
      EmployeeConsultations$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class EmployeeMandatesArguments extends JsonSerializable with EquatableMixin {
  EmployeeMandatesArguments({required this.employee});

  @override
  factory EmployeeMandatesArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeMandatesArgumentsFromJson(json);

  late EmployeeMandateInput employee;

  @override
  List<Object?> get props => [employee];
  @override
  Map<String, dynamic> toJson() => _$EmployeeMandatesArgumentsToJson(this);
}

final EMPLOYEE_MANDATES_QUERY_DOCUMENT_OPERATION_NAME = 'employeeMandates';
final EMPLOYEE_MANDATES_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'employeeMandates'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'employee')),
            type: NamedTypeNode(
                name: NameNode(value: 'EmployeeMandateInput'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeMandates'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'employee'),
                  value: VariableNode(name: NameNode(value: 'employee')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'mandateId'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'description'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'candidates'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FragmentSpreadNode(
                        name: NameNode(value: 'EmployeeCandidateFragment'),
                        directives: [])
                  ]))
            ]))
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeeCandidateFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'EmployeeCandidate'), isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'id'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'person'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FragmentSpreadNode(
                  name: NameNode(value: 'EmployeePersonFragment'),
                  directives: [])
            ]))
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeePersonFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'EmployeePerson'), isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'id'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'name'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'registration'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'suggestion'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'profilePicture'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null)
      ]))
]);

class EmployeeMandatesQuery
    extends GraphQLQuery<EmployeeMandates$Query, EmployeeMandatesArguments> {
  EmployeeMandatesQuery({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_MANDATES_QUERY_DOCUMENT;

  @override
  final String operationName = EMPLOYEE_MANDATES_QUERY_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeMandatesArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeMandates$Query parse(Map<String, dynamic> json) =>
      EmployeeMandates$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class EmployeeLoginArguments extends JsonSerializable with EquatableMixin {
  EmployeeLoginArguments({required this.generate});

  @override
  factory EmployeeLoginArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeLoginArgumentsFromJson(json);

  late BaseInput generate;

  @override
  List<Object?> get props => [generate];
  @override
  Map<String, dynamic> toJson() => _$EmployeeLoginArgumentsToJson(this);
}

final EMPLOYEE_LOGIN_QUERY_DOCUMENT_OPERATION_NAME = 'employeeLogin';
final EMPLOYEE_LOGIN_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'employeeLogin'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'generate')),
            type: NamedTypeNode(
                name: NameNode(value: 'BaseInput'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeLogin'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'generate'),
                  value: VariableNode(name: NameNode(value: 'generate')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'isActive'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'socialName'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'generalRegistration'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'email'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'registration'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'user'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'profilePicture'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'uploadUrl'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'admissionDate'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'pisCode'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'name'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'educationLevel'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'individualRegistration'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'birthday'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'gender'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'department'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ])),
              FieldNode(
                  name: NameNode(value: 'occupation'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FieldNode(
                        name: NameNode(value: 'description'),
                        alias: null,
                        arguments: [],
                        directives: [],
                        selectionSet: null)
                  ]))
            ]))
      ]))
]);

class EmployeeLoginQuery
    extends GraphQLQuery<EmployeeLogin$Query, EmployeeLoginArguments> {
  EmployeeLoginQuery({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_LOGIN_QUERY_DOCUMENT;

  @override
  final String operationName = EMPLOYEE_LOGIN_QUERY_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeLoginArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeLogin$Query parse(Map<String, dynamic> json) =>
      EmployeeLogin$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class EmployeeHasCandidateArguments extends JsonSerializable
    with EquatableMixin {
  EmployeeHasCandidateArguments({required this.employee});

  @override
  factory EmployeeHasCandidateArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeHasCandidateArgumentsFromJson(json);

  late EmployeeHasCandidateInput employee;

  @override
  List<Object?> get props => [employee];
  @override
  Map<String, dynamic> toJson() => _$EmployeeHasCandidateArgumentsToJson(this);
}

final EMPLOYEE_HAS_CANDIDATE_QUERY_DOCUMENT_OPERATION_NAME =
    'employeeHasCandidate';
final EMPLOYEE_HAS_CANDIDATE_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'employeeHasCandidate'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'employee')),
            type: NamedTypeNode(
                name: NameNode(value: 'EmployeeHasCandidateInput'),
                isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeHasCandidate'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'employee'),
                  value: VariableNode(name: NameNode(value: 'employee')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'id'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null)
            ]))
      ]))
]);

class EmployeeHasCandidateQuery extends GraphQLQuery<EmployeeHasCandidate$Query,
    EmployeeHasCandidateArguments> {
  EmployeeHasCandidateQuery({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_HAS_CANDIDATE_QUERY_DOCUMENT;

  @override
  final String operationName =
      EMPLOYEE_HAS_CANDIDATE_QUERY_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeHasCandidateArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeHasCandidate$Query parse(Map<String, dynamic> json) =>
      EmployeeHasCandidate$Query.fromJson(json);
}

@JsonSerializable(explicitToJson: true)
class EmployeeRisksArguments extends JsonSerializable with EquatableMixin {
  EmployeeRisksArguments({required this.employee});

  @override
  factory EmployeeRisksArguments.fromJson(Map<String, dynamic> json) =>
      _$EmployeeRisksArgumentsFromJson(json);

  late BaseInput employee;

  @override
  List<Object?> get props => [employee];
  @override
  Map<String, dynamic> toJson() => _$EmployeeRisksArgumentsToJson(this);
}

final EMPLOYEE_RISKS_QUERY_DOCUMENT_OPERATION_NAME = 'employeeRisks';
final EMPLOYEE_RISKS_QUERY_DOCUMENT = DocumentNode(definitions: [
  OperationDefinitionNode(
      type: OperationType.query,
      name: NameNode(value: 'employeeRisks'),
      variableDefinitions: [
        VariableDefinitionNode(
            variable: VariableNode(name: NameNode(value: 'employee')),
            type: NamedTypeNode(
                name: NameNode(value: 'BaseInput'), isNonNull: true),
            defaultValue: DefaultValueNode(value: null),
            directives: [])
      ],
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: 'employeeRisks'),
            alias: null,
            arguments: [
              ArgumentNode(
                  name: NameNode(value: 'employee'),
                  value: VariableNode(name: NameNode(value: 'employee')))
            ],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                  name: NameNode(value: 'recognitionDate'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'riskAgent'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'riskGroup'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'generationSource'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'physicalEnvironment'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'measureType'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'unitMeasurement'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'actionLevel'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'exposureTime'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'repetition'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'interval'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'riskDegree'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'riskMap'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'category'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'probability'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'severity'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'protectionMeasure'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'workingCondition'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'uninterruptedUse'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'expirationDate'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'exchangePeriodicity'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'sanitation'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'needIpe'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'needCpe'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: null),
              FieldNode(
                  name: NameNode(value: 'ipes'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FragmentSpreadNode(
                        name: NameNode(value: 'EmployeeRiskIpeFragment'),
                        directives: [])
                  ])),
              FieldNode(
                  name: NameNode(value: 'controlMeasures'),
                  alias: null,
                  arguments: [],
                  directives: [],
                  selectionSet: SelectionSetNode(selections: [
                    FragmentSpreadNode(
                        name: NameNode(value: 'EmployeeControlMeasureFragment'),
                        directives: [])
                  ]))
            ]))
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeeRiskIpeFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'EmployeeRiskIpe'), isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'description'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'change'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null)
      ])),
  FragmentDefinitionNode(
      name: NameNode(value: 'EmployeeControlMeasureFragment'),
      typeCondition: TypeConditionNode(
          on: NamedTypeNode(
              name: NameNode(value: 'EmployeeControlMeasure'),
              isNonNull: false)),
      directives: [],
      selectionSet: SelectionSetNode(selections: [
        FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null),
        FieldNode(
            name: NameNode(value: 'description'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null)
      ]))
]);

class EmployeeRisksQuery
    extends GraphQLQuery<EmployeeRisks$Query, EmployeeRisksArguments> {
  EmployeeRisksQuery({required this.variables});

  @override
  final DocumentNode document = EMPLOYEE_RISKS_QUERY_DOCUMENT;

  @override
  final String operationName = EMPLOYEE_RISKS_QUERY_DOCUMENT_OPERATION_NAME;

  @override
  final EmployeeRisksArguments variables;

  @override
  List<Object?> get props => [document, operationName, variables];
  @override
  EmployeeRisks$Query parse(Map<String, dynamic> json) =>
      EmployeeRisks$Query.fromJson(json);
}
