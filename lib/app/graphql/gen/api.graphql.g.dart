// GENERATED CODE - DO NOT MODIFY BY HAND
// @dart=2.12

part of 'api.graphql.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmployeeVote$Mutation$EmployeeVote _$EmployeeVote$Mutation$EmployeeVoteFromJson(
        Map<String, dynamic> json) =>
    EmployeeVote$Mutation$EmployeeVote()
      ..id = json['id'] as String
      ..mandateId = json['mandateId'] as String;

Map<String, dynamic> _$EmployeeVote$Mutation$EmployeeVoteToJson(
        EmployeeVote$Mutation$EmployeeVote instance) =>
    <String, dynamic>{
      'id': instance.id,
      'mandateId': instance.mandateId,
    };

EmployeeVote$Mutation _$EmployeeVote$MutationFromJson(
        Map<String, dynamic> json) =>
    EmployeeVote$Mutation()
      ..employeeVote = EmployeeVote$Mutation$EmployeeVote.fromJson(
          json['employeeVote'] as Map<String, dynamic>);

Map<String, dynamic> _$EmployeeVote$MutationToJson(
        EmployeeVote$Mutation instance) =>
    <String, dynamic>{
      'employeeVote': instance.employeeVote.toJson(),
    };

EmployeeVoteInput _$EmployeeVoteInputFromJson(Map<String, dynamic> json) =>
    EmployeeVoteInput(
      organizationId: json['organizationId'] as String,
      individualRegistration: json['individualRegistration'] as String,
      password: json['password'] as String,
      mandateId: json['mandateId'] as String,
      candidateId: json['candidateId'] as String?,
      nullVote: json['nullVote'] as bool?,
      whiteVote: json['whiteVote'] as bool?,
    );

Map<String, dynamic> _$EmployeeVoteInputToJson(EmployeeVoteInput instance) =>
    <String, dynamic>{
      'organizationId': instance.organizationId,
      'individualRegistration': instance.individualRegistration,
      'password': instance.password,
      'mandateId': instance.mandateId,
      'candidateId': instance.candidateId,
      'nullVote': instance.nullVote,
      'whiteVote': instance.whiteVote,
    };

EmployeeCandidate$Mutation$EmployeeCandidate
    _$EmployeeCandidate$Mutation$EmployeeCandidateFromJson(
            Map<String, dynamic> json) =>
        EmployeeCandidate$Mutation$EmployeeCandidate()
          ..id = json['id'] as String;

Map<String, dynamic> _$EmployeeCandidate$Mutation$EmployeeCandidateToJson(
        EmployeeCandidate$Mutation$EmployeeCandidate instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

EmployeeCandidate$Mutation _$EmployeeCandidate$MutationFromJson(
        Map<String, dynamic> json) =>
    EmployeeCandidate$Mutation()
      ..employeeCandidate =
          EmployeeCandidate$Mutation$EmployeeCandidate.fromJson(
              json['employeeCandidate'] as Map<String, dynamic>);

Map<String, dynamic> _$EmployeeCandidate$MutationToJson(
        EmployeeCandidate$Mutation instance) =>
    <String, dynamic>{
      'employeeCandidate': instance.employeeCandidate.toJson(),
    };

EmployeeCandidateInput _$EmployeeCandidateInputFromJson(
        Map<String, dynamic> json) =>
    EmployeeCandidateInput(
      organizationId: json['organizationId'] as String,
      individualRegistration: json['individualRegistration'] as String,
      password: json['password'] as String,
      mandateId: json['mandateId'] as String,
    );

Map<String, dynamic> _$EmployeeCandidateInputToJson(
        EmployeeCandidateInput instance) =>
    <String, dynamic>{
      'organizationId': instance.organizationId,
      'individualRegistration': instance.individualRegistration,
      'password': instance.password,
      'mandateId': instance.mandateId,
    };

GeneratePassword$Mutation$GeneratePassword
    _$GeneratePassword$Mutation$GeneratePasswordFromJson(
            Map<String, dynamic> json) =>
        GeneratePassword$Mutation$GeneratePassword()
          ..email = json['email'] as String?
          ..user = json['user'] as String?
          ..socialName = json['socialName'] as String?;

Map<String, dynamic> _$GeneratePassword$Mutation$GeneratePasswordToJson(
        GeneratePassword$Mutation$GeneratePassword instance) =>
    <String, dynamic>{
      'email': instance.email,
      'user': instance.user,
      'socialName': instance.socialName,
    };

GeneratePassword$Mutation _$GeneratePassword$MutationFromJson(
        Map<String, dynamic> json) =>
    GeneratePassword$Mutation()
      ..generatePassword = GeneratePassword$Mutation$GeneratePassword.fromJson(
          json['generatePassword'] as Map<String, dynamic>);

Map<String, dynamic> _$GeneratePassword$MutationToJson(
        GeneratePassword$Mutation instance) =>
    <String, dynamic>{
      'generatePassword': instance.generatePassword.toJson(),
    };

GeneratePasswordInput _$GeneratePasswordInputFromJson(
        Map<String, dynamic> json) =>
    GeneratePasswordInput(
      organizationId: json['organizationId'] as String,
      individualRegistration: json['individualRegistration'] as String,
      password: json['password'] as String,
      registration: json['registration'] as String,
      birthday: fromGraphQLDateTimeToDartDateTime(json['birthday'] as String),
      operation: $enumDecode(_$EmployeeOperationTypeEnumMap, json['operation'],
          unknownValue: EmployeeOperationType.artemisUnknown),
    );

Map<String, dynamic> _$GeneratePasswordInputToJson(
        GeneratePasswordInput instance) =>
    <String, dynamic>{
      'organizationId': instance.organizationId,
      'individualRegistration': instance.individualRegistration,
      'password': instance.password,
      'registration': instance.registration,
      'birthday': fromDartDateTimeToGraphQLDateTime(instance.birthday),
      'operation': _$EmployeeOperationTypeEnumMap[instance.operation],
    };

const _$EmployeeOperationTypeEnumMap = {
  EmployeeOperationType.create: 'Create',
  EmployeeOperationType.change: 'Change',
  EmployeeOperationType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

EmployeeIpes$Query$EmployeeIpes _$EmployeeIpes$Query$EmployeeIpesFromJson(
        Map<String, dynamic> json) =>
    EmployeeIpes$Query$EmployeeIpes()
      ..equipmentId = json['equipmentId'] as String
      ..equipmentDescription = json['equipmentDescription'] as String
      ..nextChange = fromGraphQLDateTimeNullableToDartDateTimeNullable(
          json['nextChange'] as String?)
      ..caCode = json['caCode'] as String?
      ..date = fromGraphQLDateTimeNullableToDartDateTimeNullable(
          json['date'] as String?)
      ..lastMaintenance = fromGraphQLDateTimeNullableToDartDateTimeNullable(
          json['lastMaintenance'] as String?)
      ..reason = $enumDecodeNullable(_$DeliveryReasonEnumMap, json['reason'],
          unknownValue: DeliveryReason.artemisUnknown)
      ..amount = (json['amount'] as num?)?.toDouble()
      ..effective = json['effective'] as bool?
      ..observation = json['observation'] as String?;

Map<String, dynamic> _$EmployeeIpes$Query$EmployeeIpesToJson(
        EmployeeIpes$Query$EmployeeIpes instance) =>
    <String, dynamic>{
      'equipmentId': instance.equipmentId,
      'equipmentDescription': instance.equipmentDescription,
      'nextChange': fromDartDateTimeNullableToGraphQLDateTimeNullable(
          instance.nextChange),
      'caCode': instance.caCode,
      'date': fromDartDateTimeNullableToGraphQLDateTimeNullable(instance.date),
      'lastMaintenance': fromDartDateTimeNullableToGraphQLDateTimeNullable(
          instance.lastMaintenance),
      'reason': _$DeliveryReasonEnumMap[instance.reason],
      'amount': instance.amount,
      'effective': instance.effective,
      'observation': instance.observation,
    };

const _$DeliveryReasonEnumMap = {
  DeliveryReason.admissional: 'Admissional',
  DeliveryReason.wear: 'Wear',
  DeliveryReason.defect: 'Defect',
  DeliveryReason.loss: 'Loss',
  DeliveryReason.theft: 'Theft',
  DeliveryReason.demissional: 'Demissional',
  DeliveryReason.sanitation: 'Sanitation',
  DeliveryReason.functionChange: 'FunctionChange',
  DeliveryReason.dielectricTest: 'DielectricTest',
  DeliveryReason.other: 'Other',
  DeliveryReason.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

EmployeeIpes$Query _$EmployeeIpes$QueryFromJson(Map<String, dynamic> json) =>
    EmployeeIpes$Query()
      ..employeeIpes = (json['employeeIpes'] as List<dynamic>)
          .map((e) => EmployeeIpes$Query$EmployeeIpes.fromJson(
              e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$EmployeeIpes$QueryToJson(EmployeeIpes$Query instance) =>
    <String, dynamic>{
      'employeeIpes': instance.employeeIpes.map((e) => e.toJson()).toList(),
    };

BaseInput _$BaseInputFromJson(Map<String, dynamic> json) => BaseInput(
      organizationId: json['organizationId'] as String,
      individualRegistration: json['individualRegistration'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$BaseInputToJson(BaseInput instance) => <String, dynamic>{
      'organizationId': instance.organizationId,
      'individualRegistration': instance.individualRegistration,
      'password': instance.password,
    };

EmployeeHasVote$Query$EmployeeHasVote
    _$EmployeeHasVote$Query$EmployeeHasVoteFromJson(
            Map<String, dynamic> json) =>
        EmployeeHasVote$Query$EmployeeHasVote()
          ..id = json['id'] as String
          ..mandateId = json['mandateId'] as String
          ..cypherUser = json['cypherUser'] as String
          ..nullVote = json['nullVote'] as bool?
          ..whiteVote = json['whiteVote'] as bool?
          ..candidateId = json['candidateId'] as String?;

Map<String, dynamic> _$EmployeeHasVote$Query$EmployeeHasVoteToJson(
        EmployeeHasVote$Query$EmployeeHasVote instance) =>
    <String, dynamic>{
      'id': instance.id,
      'mandateId': instance.mandateId,
      'cypherUser': instance.cypherUser,
      'nullVote': instance.nullVote,
      'whiteVote': instance.whiteVote,
      'candidateId': instance.candidateId,
    };

EmployeeHasVote$Query _$EmployeeHasVote$QueryFromJson(
        Map<String, dynamic> json) =>
    EmployeeHasVote$Query()
      ..employeeHasVote = json['employeeHasVote'] == null
          ? null
          : EmployeeHasVote$Query$EmployeeHasVote.fromJson(
              json['employeeHasVote'] as Map<String, dynamic>);

Map<String, dynamic> _$EmployeeHasVote$QueryToJson(
        EmployeeHasVote$Query instance) =>
    <String, dynamic>{
      'employeeHasVote': instance.employeeHasVote?.toJson(),
    };

EmployeeHasVoteInput _$EmployeeHasVoteInputFromJson(
        Map<String, dynamic> json) =>
    EmployeeHasVoteInput(
      organizationId: json['organizationId'] as String,
      individualRegistration: json['individualRegistration'] as String,
      password: json['password'] as String,
      mandateId: json['mandateId'] as String,
    );

Map<String, dynamic> _$EmployeeHasVoteInputToJson(
        EmployeeHasVoteInput instance) =>
    <String, dynamic>{
      'organizationId': instance.organizationId,
      'individualRegistration': instance.individualRegistration,
      'password': instance.password,
      'mandateId': instance.mandateId,
    };

EmployeeInfo$Query$EmployeeInfo$CostCenter
    _$EmployeeInfo$Query$EmployeeInfo$CostCenterFromJson(
            Map<String, dynamic> json) =>
        EmployeeInfo$Query$EmployeeInfo$CostCenter()
          ..$$typename = json['__typename'] as String?
          ..description = json['description'] as String;

Map<String, dynamic> _$EmployeeInfo$Query$EmployeeInfo$CostCenterToJson(
        EmployeeInfo$Query$EmployeeInfo$CostCenter instance) =>
    <String, dynamic>{
      '__typename': instance.$$typename,
      'description': instance.description,
    };

EmployeeInfo$Query$EmployeeInfo$Department
    _$EmployeeInfo$Query$EmployeeInfo$DepartmentFromJson(
            Map<String, dynamic> json) =>
        EmployeeInfo$Query$EmployeeInfo$Department()
          ..$$typename = json['__typename'] as String?
          ..description = json['description'] as String;

Map<String, dynamic> _$EmployeeInfo$Query$EmployeeInfo$DepartmentToJson(
        EmployeeInfo$Query$EmployeeInfo$Department instance) =>
    <String, dynamic>{
      '__typename': instance.$$typename,
      'description': instance.description,
    };

EmployeeInfo$Query$EmployeeInfo$Occupation
    _$EmployeeInfo$Query$EmployeeInfo$OccupationFromJson(
            Map<String, dynamic> json) =>
        EmployeeInfo$Query$EmployeeInfo$Occupation()
          ..$$typename = json['__typename'] as String?
          ..description = json['description'] as String;

Map<String, dynamic> _$EmployeeInfo$Query$EmployeeInfo$OccupationToJson(
        EmployeeInfo$Query$EmployeeInfo$Occupation instance) =>
    <String, dynamic>{
      '__typename': instance.$$typename,
      'description': instance.description,
    };

EmployeeInfo$Query$EmployeeInfo$WorkShift
    _$EmployeeInfo$Query$EmployeeInfo$WorkShiftFromJson(
            Map<String, dynamic> json) =>
        EmployeeInfo$Query$EmployeeInfo$WorkShift()
          ..$$typename = json['__typename'] as String?
          ..description = json['description'] as String;

Map<String, dynamic> _$EmployeeInfo$Query$EmployeeInfo$WorkShiftToJson(
        EmployeeInfo$Query$EmployeeInfo$WorkShift instance) =>
    <String, dynamic>{
      '__typename': instance.$$typename,
      'description': instance.description,
    };

EmployeeInfo$Query$EmployeeInfo$Position
    _$EmployeeInfo$Query$EmployeeInfo$PositionFromJson(
            Map<String, dynamic> json) =>
        EmployeeInfo$Query$EmployeeInfo$Position()
          ..$$typename = json['__typename'] as String?
          ..description = json['description'] as String;

Map<String, dynamic> _$EmployeeInfo$Query$EmployeeInfo$PositionToJson(
        EmployeeInfo$Query$EmployeeInfo$Position instance) =>
    <String, dynamic>{
      '__typename': instance.$$typename,
      'description': instance.description,
    };

EmployeeInfo$Query$EmployeeInfo _$EmployeeInfo$Query$EmployeeInfoFromJson(
        Map<String, dynamic> json) =>
    EmployeeInfo$Query$EmployeeInfo()
      ..profilePicture = json['profilePicture'] as String?
      ..name = json['name'] as String
      ..email = json['email'] as String?
      ..gender = $enumDecode(_$GenderEnumMap, json['gender'],
          unknownValue: Gender.artemisUnknown)
      ..address = json['address'] as String?
      ..individualRegistration = json['individualRegistration'] as String
      ..educationLevel = $enumDecodeNullable(
          _$EducationLevelEnumMap, json['educationLevel'],
          unknownValue: EducationLevel.artemisUnknown)
      ..registration = json['registration'] as String?
      ..costCenter = json['costCenter'] == null
          ? null
          : EmployeeInfo$Query$EmployeeInfo$CostCenter.fromJson(
              json['costCenter'] as Map<String, dynamic>)
      ..department = json['department'] == null
          ? null
          : EmployeeInfo$Query$EmployeeInfo$Department.fromJson(
              json['department'] as Map<String, dynamic>)
      ..occupation = json['occupation'] == null
          ? null
          : EmployeeInfo$Query$EmployeeInfo$Occupation.fromJson(
              json['occupation'] as Map<String, dynamic>)
      ..workShift = json['workShift'] == null
          ? null
          : EmployeeInfo$Query$EmployeeInfo$WorkShift.fromJson(
              json['workShift'] as Map<String, dynamic>)
      ..position = json['position'] == null
          ? null
          : EmployeeInfo$Query$EmployeeInfo$Position.fromJson(
              json['position'] as Map<String, dynamic>);

Map<String, dynamic> _$EmployeeInfo$Query$EmployeeInfoToJson(
        EmployeeInfo$Query$EmployeeInfo instance) =>
    <String, dynamic>{
      'profilePicture': instance.profilePicture,
      'name': instance.name,
      'email': instance.email,
      'gender': _$GenderEnumMap[instance.gender],
      'address': instance.address,
      'individualRegistration': instance.individualRegistration,
      'educationLevel': _$EducationLevelEnumMap[instance.educationLevel],
      'registration': instance.registration,
      'costCenter': instance.costCenter?.toJson(),
      'department': instance.department?.toJson(),
      'occupation': instance.occupation?.toJson(),
      'workShift': instance.workShift?.toJson(),
      'position': instance.position?.toJson(),
    };

const _$GenderEnumMap = {
  Gender.male: 'Male',
  Gender.female: 'Female',
  Gender.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$EducationLevelEnumMap = {
  EducationLevel.elementarySchool: 'ElementarySchool',
  EducationLevel.highSchool: 'HighSchool',
  EducationLevel.universityGraduate: 'UniversityGraduate',
  EducationLevel.mastersDegree: 'MastersDegree',
  EducationLevel.doctorateDegree: 'DoctorateDegree',
  EducationLevel.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

EmployeeInfo$Query _$EmployeeInfo$QueryFromJson(Map<String, dynamic> json) =>
    EmployeeInfo$Query()
      ..employeeInfo = json['employeeInfo'] == null
          ? null
          : EmployeeInfo$Query$EmployeeInfo.fromJson(
              json['employeeInfo'] as Map<String, dynamic>);

Map<String, dynamic> _$EmployeeInfo$QueryToJson(EmployeeInfo$Query instance) =>
    <String, dynamic>{
      'employeeInfo': instance.employeeInfo?.toJson(),
    };

EmployeeConsultations$Query$EmployeeConsultations$Exams
    _$EmployeeConsultations$Query$EmployeeConsultations$ExamsFromJson(
            Map<String, dynamic> json) =>
        EmployeeConsultations$Query$EmployeeConsultations$Exams()
          ..$$typename = json['__typename'] as String?
          ..name = json['name'] as String;

Map<String, dynamic>
    _$EmployeeConsultations$Query$EmployeeConsultations$ExamsToJson(
            EmployeeConsultations$Query$EmployeeConsultations$Exams instance) =>
        <String, dynamic>{
          '__typename': instance.$$typename,
          'name': instance.name,
        };

EmployeeConsultations$Query$EmployeeConsultations
    _$EmployeeConsultations$Query$EmployeeConsultationsFromJson(
            Map<String, dynamic> json) =>
        EmployeeConsultations$Query$EmployeeConsultations()
          ..doctorName = json['doctorName'] as String
          ..reason = json['reason'] as String
          ..type = $enumDecode(_$ConsultationTypeEnumMap, json['type'],
              unknownValue: ConsultationType.artemisUnknown)
          ..estimatedDate =
              fromGraphQLDateTimeToDartDateTime(json['estimatedDate'] as String)
          ..nature = $enumDecodeNullable(
              _$ConsultationNatureEnumMap, json['nature'],
              unknownValue: ConsultationNature.artemisUnknown)
          ..estimatedTime = (json['estimatedTime'] as num?)?.toDouble()
          ..startDate = fromGraphQLDateTimeNullableToDartDateTimeNullable(
              json['startDate'] as String?)
          ..endDate = fromGraphQLDateTimeNullableToDartDateTimeNullable(
              json['endDate'] as String?)
          ..accreditedName = json['accreditedName'] as String?
          ..exams = (json['exams'] as List<dynamic>)
              .map((e) =>
                  EmployeeConsultations$Query$EmployeeConsultations$Exams
                      .fromJson(e as Map<String, dynamic>))
              .toList();

Map<String, dynamic> _$EmployeeConsultations$Query$EmployeeConsultationsToJson(
        EmployeeConsultations$Query$EmployeeConsultations instance) =>
    <String, dynamic>{
      'doctorName': instance.doctorName,
      'reason': instance.reason,
      'type': _$ConsultationTypeEnumMap[instance.type],
      'estimatedDate':
          fromDartDateTimeToGraphQLDateTime(instance.estimatedDate),
      'nature': _$ConsultationNatureEnumMap[instance.nature],
      'estimatedTime': instance.estimatedTime,
      'startDate':
          fromDartDateTimeNullableToGraphQLDateTimeNullable(instance.startDate),
      'endDate':
          fromDartDateTimeNullableToGraphQLDateTimeNullable(instance.endDate),
      'accreditedName': instance.accreditedName,
      'exams': instance.exams.map((e) => e.toJson()).toList(),
    };

const _$ConsultationTypeEnumMap = {
  ConsultationType.occupational: 'Occupational',
  ConsultationType.clinical: 'Clinical',
  ConsultationType.urgency: 'Urgency',
  ConsultationType.qualityOfLife: 'QualityOfLife',
  ConsultationType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$ConsultationNatureEnumMap = {
  ConsultationNature.admissional: 'Admissional',
  ConsultationNature.periodic: 'Periodic',
  ConsultationNature.backToWork: 'BackToWork',
  ConsultationNature.changeOfFunction: 'ChangeOfFunction',
  ConsultationNature.dismissal: 'Dismissal',
  ConsultationNature.spotMonitoring: 'SpotMonitoring',
  ConsultationNature.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

EmployeeConsultations$Query _$EmployeeConsultations$QueryFromJson(
        Map<String, dynamic> json) =>
    EmployeeConsultations$Query()
      ..employeeConsultations = (json['employeeConsultations'] as List<dynamic>)
          .map((e) =>
              EmployeeConsultations$Query$EmployeeConsultations.fromJson(
                  e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$EmployeeConsultations$QueryToJson(
        EmployeeConsultations$Query instance) =>
    <String, dynamic>{
      'employeeConsultations':
          instance.employeeConsultations.map((e) => e.toJson()).toList(),
    };

EmployeeMandates$Query$EmployeeMandates$Candidates
    _$EmployeeMandates$Query$EmployeeMandates$CandidatesFromJson(
            Map<String, dynamic> json) =>
        EmployeeMandates$Query$EmployeeMandates$Candidates()
          ..$$typename = json['__typename'] as String?
          ..id = json['id'] as String
          ..person = EmployeeCandidateFragmentMixin$Person.fromJson(
              json['person'] as Map<String, dynamic>);

Map<String, dynamic> _$EmployeeMandates$Query$EmployeeMandates$CandidatesToJson(
        EmployeeMandates$Query$EmployeeMandates$Candidates instance) =>
    <String, dynamic>{
      '__typename': instance.$$typename,
      'id': instance.id,
      'person': instance.person.toJson(),
    };

EmployeeMandates$Query$EmployeeMandates
    _$EmployeeMandates$Query$EmployeeMandatesFromJson(
            Map<String, dynamic> json) =>
        EmployeeMandates$Query$EmployeeMandates()
          ..mandateId = json['mandateId'] as String
          ..description = json['description'] as String
          ..candidates = (json['candidates'] as List<dynamic>?)
              ?.map((e) =>
                  EmployeeMandates$Query$EmployeeMandates$Candidates.fromJson(
                      e as Map<String, dynamic>))
              .toList();

Map<String, dynamic> _$EmployeeMandates$Query$EmployeeMandatesToJson(
        EmployeeMandates$Query$EmployeeMandates instance) =>
    <String, dynamic>{
      'mandateId': instance.mandateId,
      'description': instance.description,
      'candidates': instance.candidates?.map((e) => e.toJson()).toList(),
    };

EmployeeMandates$Query _$EmployeeMandates$QueryFromJson(
        Map<String, dynamic> json) =>
    EmployeeMandates$Query()
      ..employeeMandates = (json['employeeMandates'] as List<dynamic>)
          .map((e) => EmployeeMandates$Query$EmployeeMandates.fromJson(
              e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$EmployeeMandates$QueryToJson(
        EmployeeMandates$Query instance) =>
    <String, dynamic>{
      'employeeMandates':
          instance.employeeMandates.map((e) => e.toJson()).toList(),
    };

EmployeeCandidateFragmentMixin$Person
    _$EmployeeCandidateFragmentMixin$PersonFromJson(
            Map<String, dynamic> json) =>
        EmployeeCandidateFragmentMixin$Person()
          ..$$typename = json['__typename'] as String?
          ..id = json['id'] as String
          ..name = json['name'] as String
          ..registration = json['registration'] as String?
          ..suggestion = json['suggestion'] as String?
          ..profilePicture = json['profilePicture'] as String?;

Map<String, dynamic> _$EmployeeCandidateFragmentMixin$PersonToJson(
        EmployeeCandidateFragmentMixin$Person instance) =>
    <String, dynamic>{
      '__typename': instance.$$typename,
      'id': instance.id,
      'name': instance.name,
      'registration': instance.registration,
      'suggestion': instance.suggestion,
      'profilePicture': instance.profilePicture,
    };

EmployeeMandateInput _$EmployeeMandateInputFromJson(
        Map<String, dynamic> json) =>
    EmployeeMandateInput(
      organizationId: json['organizationId'] as String,
      individualRegistration: json['individualRegistration'] as String,
      password: json['password'] as String,
      type: $enumDecode(_$MandateEventTypeEnumMap, json['type'],
          unknownValue: MandateEventType.artemisUnknown),
    );

Map<String, dynamic> _$EmployeeMandateInputToJson(
        EmployeeMandateInput instance) =>
    <String, dynamic>{
      'organizationId': instance.organizationId,
      'individualRegistration': instance.individualRegistration,
      'password': instance.password,
      'type': _$MandateEventTypeEnumMap[instance.type],
    };

const _$MandateEventTypeEnumMap = {
  MandateEventType.candidate: 'Candidate',
  MandateEventType.vote: 'Vote',
  MandateEventType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

EmployeeLogin$Query$EmployeeLogin$Department
    _$EmployeeLogin$Query$EmployeeLogin$DepartmentFromJson(
            Map<String, dynamic> json) =>
        EmployeeLogin$Query$EmployeeLogin$Department()
          ..description = json['description'] as String;

Map<String, dynamic> _$EmployeeLogin$Query$EmployeeLogin$DepartmentToJson(
        EmployeeLogin$Query$EmployeeLogin$Department instance) =>
    <String, dynamic>{
      'description': instance.description,
    };

EmployeeLogin$Query$EmployeeLogin$Occupation
    _$EmployeeLogin$Query$EmployeeLogin$OccupationFromJson(
            Map<String, dynamic> json) =>
        EmployeeLogin$Query$EmployeeLogin$Occupation()
          ..description = json['description'] as String;

Map<String, dynamic> _$EmployeeLogin$Query$EmployeeLogin$OccupationToJson(
        EmployeeLogin$Query$EmployeeLogin$Occupation instance) =>
    <String, dynamic>{
      'description': instance.description,
    };

EmployeeLogin$Query$EmployeeLogin _$EmployeeLogin$Query$EmployeeLoginFromJson(
        Map<String, dynamic> json) =>
    EmployeeLogin$Query$EmployeeLogin()
      ..id = json['id'] as String
      ..isActive = json['isActive'] as bool
      ..socialName = json['socialName'] as String?
      ..generalRegistration = json['generalRegistration'] as String?
      ..email = json['email'] as String?
      ..registration = json['registration'] as String?
      ..user = json['user'] as String?
      ..profilePicture = json['profilePicture'] as String?
      ..uploadUrl = json['uploadUrl'] as String?
      ..admissionDate = fromGraphQLDateTimeNullableToDartDateTimeNullable(
          json['admissionDate'] as String?)
      ..pisCode = json['pisCode'] as String?
      ..name = json['name'] as String
      ..educationLevel = $enumDecodeNullable(
          _$EducationLevelEnumMap, json['educationLevel'],
          unknownValue: EducationLevel.artemisUnknown)
      ..individualRegistration = json['individualRegistration'] as String
      ..birthday = fromGraphQLDateTimeToDartDateTime(json['birthday'] as String)
      ..gender = $enumDecode(_$GenderEnumMap, json['gender'],
          unknownValue: Gender.artemisUnknown)
      ..department = json['department'] == null
          ? null
          : EmployeeLogin$Query$EmployeeLogin$Department.fromJson(
              json['department'] as Map<String, dynamic>)
      ..occupation = json['occupation'] == null
          ? null
          : EmployeeLogin$Query$EmployeeLogin$Occupation.fromJson(
              json['occupation'] as Map<String, dynamic>);

Map<String, dynamic> _$EmployeeLogin$Query$EmployeeLoginToJson(
        EmployeeLogin$Query$EmployeeLogin instance) =>
    <String, dynamic>{
      'id': instance.id,
      'isActive': instance.isActive,
      'socialName': instance.socialName,
      'generalRegistration': instance.generalRegistration,
      'email': instance.email,
      'registration': instance.registration,
      'user': instance.user,
      'profilePicture': instance.profilePicture,
      'uploadUrl': instance.uploadUrl,
      'admissionDate': fromDartDateTimeNullableToGraphQLDateTimeNullable(
          instance.admissionDate),
      'pisCode': instance.pisCode,
      'name': instance.name,
      'educationLevel': _$EducationLevelEnumMap[instance.educationLevel],
      'individualRegistration': instance.individualRegistration,
      'birthday': fromDartDateTimeToGraphQLDateTime(instance.birthday),
      'gender': _$GenderEnumMap[instance.gender],
      'department': instance.department?.toJson(),
      'occupation': instance.occupation?.toJson(),
    };

EmployeeLogin$Query _$EmployeeLogin$QueryFromJson(Map<String, dynamic> json) =>
    EmployeeLogin$Query()
      ..employeeLogin = EmployeeLogin$Query$EmployeeLogin.fromJson(
          json['employeeLogin'] as Map<String, dynamic>);

Map<String, dynamic> _$EmployeeLogin$QueryToJson(
        EmployeeLogin$Query instance) =>
    <String, dynamic>{
      'employeeLogin': instance.employeeLogin.toJson(),
    };

EmployeeHasCandidate$Query$EmployeeHasCandidate
    _$EmployeeHasCandidate$Query$EmployeeHasCandidateFromJson(
            Map<String, dynamic> json) =>
        EmployeeHasCandidate$Query$EmployeeHasCandidate()
          ..id = json['id'] as String;

Map<String, dynamic> _$EmployeeHasCandidate$Query$EmployeeHasCandidateToJson(
        EmployeeHasCandidate$Query$EmployeeHasCandidate instance) =>
    <String, dynamic>{
      'id': instance.id,
    };

EmployeeHasCandidate$Query _$EmployeeHasCandidate$QueryFromJson(
        Map<String, dynamic> json) =>
    EmployeeHasCandidate$Query()
      ..employeeHasCandidate = json['employeeHasCandidate'] == null
          ? null
          : EmployeeHasCandidate$Query$EmployeeHasCandidate.fromJson(
              json['employeeHasCandidate'] as Map<String, dynamic>);

Map<String, dynamic> _$EmployeeHasCandidate$QueryToJson(
        EmployeeHasCandidate$Query instance) =>
    <String, dynamic>{
      'employeeHasCandidate': instance.employeeHasCandidate?.toJson(),
    };

EmployeeHasCandidateInput _$EmployeeHasCandidateInputFromJson(
        Map<String, dynamic> json) =>
    EmployeeHasCandidateInput(
      organizationId: json['organizationId'] as String,
      individualRegistration: json['individualRegistration'] as String,
      password: json['password'] as String,
      mandateId: json['mandateId'] as String,
    );

Map<String, dynamic> _$EmployeeHasCandidateInputToJson(
        EmployeeHasCandidateInput instance) =>
    <String, dynamic>{
      'organizationId': instance.organizationId,
      'individualRegistration': instance.individualRegistration,
      'password': instance.password,
      'mandateId': instance.mandateId,
    };

EmployeeRisks$Query$EmployeeRisks$Ipes
    _$EmployeeRisks$Query$EmployeeRisks$IpesFromJson(
            Map<String, dynamic> json) =>
        EmployeeRisks$Query$EmployeeRisks$Ipes()
          ..$$typename = json['__typename'] as String?
          ..description = json['description'] as String
          ..change = (json['change'] as num).toDouble();

Map<String, dynamic> _$EmployeeRisks$Query$EmployeeRisks$IpesToJson(
        EmployeeRisks$Query$EmployeeRisks$Ipes instance) =>
    <String, dynamic>{
      '__typename': instance.$$typename,
      'description': instance.description,
      'change': instance.change,
    };

EmployeeRisks$Query$EmployeeRisks$ControlMeasures
    _$EmployeeRisks$Query$EmployeeRisks$ControlMeasuresFromJson(
            Map<String, dynamic> json) =>
        EmployeeRisks$Query$EmployeeRisks$ControlMeasures()
          ..$$typename = json['__typename'] as String?
          ..description = json['description'] as String;

Map<String, dynamic> _$EmployeeRisks$Query$EmployeeRisks$ControlMeasuresToJson(
        EmployeeRisks$Query$EmployeeRisks$ControlMeasures instance) =>
    <String, dynamic>{
      '__typename': instance.$$typename,
      'description': instance.description,
    };

EmployeeRisks$Query$EmployeeRisks _$EmployeeRisks$Query$EmployeeRisksFromJson(
        Map<String, dynamic> json) =>
    EmployeeRisks$Query$EmployeeRisks()
      ..recognitionDate =
          fromGraphQLDateTimeToDartDateTime(json['recognitionDate'] as String)
      ..riskAgent = json['riskAgent'] as String
      ..riskGroup = $enumDecode(_$AgentGroupEnumMap, json['riskGroup'],
          unknownValue: AgentGroup.artemisUnknown)
      ..generationSource = json['generationSource'] as String
      ..physicalEnvironment = json['physicalEnvironment'] as String
      ..measureType = $enumDecodeNullable(
          _$RiskMeasureTypeEnumMap, json['measureType'],
          unknownValue: RiskMeasureType.artemisUnknown)
      ..unitMeasurement = $enumDecodeNullable(
          _$RiskMeasureEnumMap, json['unitMeasurement'],
          unknownValue: RiskMeasure.artemisUnknown)
      ..actionLevel = (json['actionLevel'] as num?)?.toDouble()
      ..exposureTime = json['exposureTime'] as String?
      ..repetition = json['repetition'] as String?
      ..interval = json['interval'] as String?
      ..riskDegree = $enumDecodeNullable(
          _$RiskDegreeEnumMap, json['riskDegree'],
          unknownValue: RiskDegree.artemisUnknown)
      ..riskMap = $enumDecodeNullable(_$RiskMapEnumMap, json['riskMap'],
          unknownValue: RiskMap.artemisUnknown)
      ..category = $enumDecodeNullable(_$RiskCategoryEnumMap, json['category'],
          unknownValue: RiskCategory.artemisUnknown)
      ..probability = $enumDecodeNullable(
          _$RiskProbabilityEnumMap, json['probability'],
          unknownValue: RiskProbability.artemisUnknown)
      ..severity = $enumDecodeNullable(_$RiskSeverityEnumMap, json['severity'],
          unknownValue: RiskSeverity.artemisUnknown)
      ..protectionMeasure = json['protectionMeasure'] as bool
      ..workingCondition = json['workingCondition'] as bool
      ..uninterruptedUse = json['uninterruptedUse'] as bool
      ..expirationDate = json['expirationDate'] as bool
      ..exchangePeriodicity = json['exchangePeriodicity'] as bool
      ..sanitation = json['sanitation'] as bool
      ..needIpe = json['needIpe'] as bool?
      ..needCpe = json['needCpe'] as bool?
      ..ipes = (json['ipes'] as List<dynamic>)
          .map((e) => EmployeeRisks$Query$EmployeeRisks$Ipes.fromJson(
              e as Map<String, dynamic>))
          .toList()
      ..controlMeasures = (json['controlMeasures'] as List<dynamic>)
          .map((e) =>
              EmployeeRisks$Query$EmployeeRisks$ControlMeasures.fromJson(
                  e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$EmployeeRisks$Query$EmployeeRisksToJson(
        EmployeeRisks$Query$EmployeeRisks instance) =>
    <String, dynamic>{
      'recognitionDate':
          fromDartDateTimeToGraphQLDateTime(instance.recognitionDate),
      'riskAgent': instance.riskAgent,
      'riskGroup': _$AgentGroupEnumMap[instance.riskGroup],
      'generationSource': instance.generationSource,
      'physicalEnvironment': instance.physicalEnvironment,
      'measureType': _$RiskMeasureTypeEnumMap[instance.measureType],
      'unitMeasurement': _$RiskMeasureEnumMap[instance.unitMeasurement],
      'actionLevel': instance.actionLevel,
      'exposureTime': instance.exposureTime,
      'repetition': instance.repetition,
      'interval': instance.interval,
      'riskDegree': _$RiskDegreeEnumMap[instance.riskDegree],
      'riskMap': _$RiskMapEnumMap[instance.riskMap],
      'category': _$RiskCategoryEnumMap[instance.category],
      'probability': _$RiskProbabilityEnumMap[instance.probability],
      'severity': _$RiskSeverityEnumMap[instance.severity],
      'protectionMeasure': instance.protectionMeasure,
      'workingCondition': instance.workingCondition,
      'uninterruptedUse': instance.uninterruptedUse,
      'expirationDate': instance.expirationDate,
      'exchangePeriodicity': instance.exchangePeriodicity,
      'sanitation': instance.sanitation,
      'needIpe': instance.needIpe,
      'needCpe': instance.needCpe,
      'ipes': instance.ipes.map((e) => e.toJson()).toList(),
      'controlMeasures':
          instance.controlMeasures.map((e) => e.toJson()).toList(),
    };

const _$AgentGroupEnumMap = {
  AgentGroup.physical: 'Physical',
  AgentGroup.chemical: 'Chemical',
  AgentGroup.biological: 'Biological',
  AgentGroup.ergonomic: 'Ergonomic',
  AgentGroup.mechanical: 'Mechanical',
  AgentGroup.dangerous: 'Dangerous',
  AgentGroup.association: 'Association',
  AgentGroup.other: 'Other',
  AgentGroup.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$RiskMeasureTypeEnumMap = {
  RiskMeasureType.above: 'above',
  RiskMeasureType.aboveOrEqual: 'aboveOrEqual',
  RiskMeasureType.equal: 'equal',
  RiskMeasureType.belowOrEqual: 'belowOrEqual',
  RiskMeasureType.below: 'below',
  RiskMeasureType.na: 'na',
  RiskMeasureType.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$RiskMeasureEnumMap = {
  RiskMeasure.numeroAdimensional: 'NumeroAdimensional',
  RiskMeasure.dBLinear: 'dBLinear',
  RiskMeasure.dBC: 'dBC',
  RiskMeasure.dBA: 'dBA',
  RiskMeasure.kCalH: 'kCalH',
  RiskMeasure.gy: 'gy',
  RiskMeasure.sv: 'sv',
  RiskMeasure.kgfCm2: 'kgfCm2',
  RiskMeasure.mS2: 'mS2',
  RiskMeasure.mS175: 'mS175',
  RiskMeasure.ppm: 'ppm',
  RiskMeasure.mgM3: 'mgM3',
  RiskMeasure.fCm3: 'fCm3',
  RiskMeasure.oC: 'oC',
  RiskMeasure.mS: 'mS',
  RiskMeasure.percentual: 'percentual',
  RiskMeasure.lx: 'lx',
  RiskMeasure.ufcM3: 'ufcM3',
  RiskMeasure.doseDiaria: 'doseDiaria',
  RiskMeasure.doseMensal: 'doseMensal',
  RiskMeasure.doseTrimestral: 'doseTrimestral',
  RiskMeasure.doseAnual: 'doseAnual',
  RiskMeasure.hz: 'hz',
  RiskMeasure.ghz: 'ghz',
  RiskMeasure.kHz: 'kHz',
  RiskMeasure.wM2: 'wM2',
  RiskMeasure.aM: 'aM',
  RiskMeasure.mT: 'mT',
  RiskMeasure.emiT: 'emiT',
  RiskMeasure.mA: 'mA',
  RiskMeasure.kvM: 'kvM',
  RiskMeasure.vM: 'vM',
  RiskMeasure.min: 'min',
  RiskMeasure.h: 'h',
  RiskMeasure.jM2: 'jM2',
  RiskMeasure.mjCm2: 'mjCm2',
  RiskMeasure.mSv: 'mSv',
  RiskMeasure.atm: 'atm',
  RiskMeasure.mppdc: 'mppdc',
  RiskMeasure.nm: 'nm',
  RiskMeasure.mW: 'mW',
  RiskMeasure.w: 'w',
  RiskMeasure.ur: 'ur',
  RiskMeasure.nA: 'nA',
  RiskMeasure.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$RiskDegreeEnumMap = {
  RiskDegree.small: 'Small',
  RiskDegree.medium: 'Medium',
  RiskDegree.large: 'Large',
  RiskDegree.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$RiskMapEnumMap = {
  RiskMap.cipa: 'CIPA',
  RiskMap.sesmt: 'SESMT',
  RiskMap.both: 'Both',
  RiskMap.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$RiskCategoryEnumMap = {
  RiskCategory.irrelevant: 'Irrelevant',
  RiskCategory.attention: 'Attention',
  RiskCategory.critical: 'Critical',
  RiskCategory.intolerable: 'Intolerable',
  RiskCategory.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$RiskProbabilityEnumMap = {
  RiskProbability.low: 'Low',
  RiskProbability.intermediate: 'Intermediate',
  RiskProbability.medium: 'Medium',
  RiskProbability.high: 'High',
  RiskProbability.veryHigh: 'VeryHigh',
  RiskProbability.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

const _$RiskSeverityEnumMap = {
  RiskSeverity.mild: 'Mild',
  RiskSeverity.intermediate: 'Intermediate',
  RiskSeverity.moderate: 'Moderate',
  RiskSeverity.severe: 'Severe',
  RiskSeverity.verySevere: 'VerySevere',
  RiskSeverity.artemisUnknown: 'ARTEMIS_UNKNOWN',
};

EmployeeRisks$Query _$EmployeeRisks$QueryFromJson(Map<String, dynamic> json) =>
    EmployeeRisks$Query()
      ..employeeRisks = (json['employeeRisks'] as List<dynamic>)
          .map((e) => EmployeeRisks$Query$EmployeeRisks.fromJson(
              e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$EmployeeRisks$QueryToJson(
        EmployeeRisks$Query instance) =>
    <String, dynamic>{
      'employeeRisks': instance.employeeRisks.map((e) => e.toJson()).toList(),
    };

EmployeeVoteArguments _$EmployeeVoteArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeVoteArguments(
      vote: EmployeeVoteInput.fromJson(json['vote'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeVoteArgumentsToJson(
        EmployeeVoteArguments instance) =>
    <String, dynamic>{
      'vote': instance.vote.toJson(),
    };

EmployeeCandidateArguments _$EmployeeCandidateArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeCandidateArguments(
      candidate: EmployeeCandidateInput.fromJson(
          json['candidate'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeCandidateArgumentsToJson(
        EmployeeCandidateArguments instance) =>
    <String, dynamic>{
      'candidate': instance.candidate.toJson(),
    };

GeneratePasswordArguments _$GeneratePasswordArgumentsFromJson(
        Map<String, dynamic> json) =>
    GeneratePasswordArguments(
      generate: GeneratePasswordInput.fromJson(
          json['generate'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GeneratePasswordArgumentsToJson(
        GeneratePasswordArguments instance) =>
    <String, dynamic>{
      'generate': instance.generate.toJson(),
    };

EmployeeIpesArguments _$EmployeeIpesArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeIpesArguments(
      employee: BaseInput.fromJson(json['employee'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeIpesArgumentsToJson(
        EmployeeIpesArguments instance) =>
    <String, dynamic>{
      'employee': instance.employee.toJson(),
    };

EmployeeHasVoteArguments _$EmployeeHasVoteArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeHasVoteArguments(
      employee: EmployeeHasVoteInput.fromJson(
          json['employee'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeHasVoteArgumentsToJson(
        EmployeeHasVoteArguments instance) =>
    <String, dynamic>{
      'employee': instance.employee.toJson(),
    };

EmployeeInfoArguments _$EmployeeInfoArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeInfoArguments(
      employee: BaseInput.fromJson(json['employee'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeInfoArgumentsToJson(
        EmployeeInfoArguments instance) =>
    <String, dynamic>{
      'employee': instance.employee.toJson(),
    };

EmployeeConsultationsArguments _$EmployeeConsultationsArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeConsultationsArguments(
      employee: BaseInput.fromJson(json['employee'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeConsultationsArgumentsToJson(
        EmployeeConsultationsArguments instance) =>
    <String, dynamic>{
      'employee': instance.employee.toJson(),
    };

EmployeeMandatesArguments _$EmployeeMandatesArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeMandatesArguments(
      employee: EmployeeMandateInput.fromJson(
          json['employee'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeMandatesArgumentsToJson(
        EmployeeMandatesArguments instance) =>
    <String, dynamic>{
      'employee': instance.employee.toJson(),
    };

EmployeeLoginArguments _$EmployeeLoginArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeLoginArguments(
      generate: BaseInput.fromJson(json['generate'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeLoginArgumentsToJson(
        EmployeeLoginArguments instance) =>
    <String, dynamic>{
      'generate': instance.generate.toJson(),
    };

EmployeeHasCandidateArguments _$EmployeeHasCandidateArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeHasCandidateArguments(
      employee: EmployeeHasCandidateInput.fromJson(
          json['employee'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeHasCandidateArgumentsToJson(
        EmployeeHasCandidateArguments instance) =>
    <String, dynamic>{
      'employee': instance.employee.toJson(),
    };

EmployeeRisksArguments _$EmployeeRisksArgumentsFromJson(
        Map<String, dynamic> json) =>
    EmployeeRisksArguments(
      employee: BaseInput.fromJson(json['employee'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EmployeeRisksArgumentsToJson(
        EmployeeRisksArguments instance) =>
    <String, dynamic>{
      'employee': instance.employee.toJson(),
    };
