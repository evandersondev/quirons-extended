import 'package:intl/intl.dart';

String toDescriptiveDateTime(DateTime date) =>
    DateFormat('dd \'de\' MMMM', 'pt-BR').format(date);

String dateTimeFormatted(DateTime date) =>
    DateFormat('dd/MM/yyyy', 'pt-BR').format(date);

String toTime(DateTime date) => DateFormat.Hm().format(date);

bool isSameDay(DateTime startDate, DateTime endDate) {
  final dayDiff = endDate.difference(startDate).inDays;

  return dayDiff == 0 && startDate.day == endDate.day;
}

bool isTomorrow(DateTime date) {
  final today = DateTime.now();
  final dayDiff = today.difference(date).inDays;

  return dayDiff == 1 && today.day == date.day;
}

bool isToday(DateTime date) {
  final today = DateTime.now();
  final dayDiff = today.difference(date).inDays;

  return dayDiff == 0 && today.day == date.day;
}

String formatRange(DateTime startDate) {
  return '''${toDescriptiveDateTime(startDate)}''';
}
