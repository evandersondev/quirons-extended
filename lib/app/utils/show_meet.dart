import 'package:intl/intl.dart';

String ShowMeet() {
  final hour = int.parse(
    DateFormat.H().format(DateTime.now()),
  );

  if (hour <= 11) {
    return 'bom dia';
  } else if (hour > 11 && hour <= 16) {
    return 'boa tarde';
  } else if (hour > 16 && hour <= 23) {
    return 'boa noite';
  } else {
    return 'bom dia';
  }
}
