import 'package:quirons_user/app/graphql/gen/api.graphql.dart';

final Map<ConsultationNature, String> ExamNatureDescription = {
  ConsultationNature.admissional: 'Admissional',
  ConsultationNature.periodic: 'Periódico',
  ConsultationNature.backToWork: 'Volta ao trabalho',
  ConsultationNature.changeOfFunction: 'Mudança de função',
  ConsultationNature.dismissal: 'Demissional',
  ConsultationNature.spotMonitoring: 'Monitorameto local',
};
