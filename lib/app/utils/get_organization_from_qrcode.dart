import 'dart:convert';

import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import '../models/organization_model.dart';

Future<OrganizationModel?> getOrganizationFromQRCode() async {
  final code = await FlutterBarcodeScanner.scanBarcode(
    '#152849',
    'Cancelar',
    false,
    ScanMode.QR,
  );

  if (code != '-1') {
    try {
      final toJson = jsonDecode(code);

      if (toJson['id'].runtimeType == String &&
          toJson['name'].runtimeType == String) {
        final org = OrganizationModel(
          id: toJson['id'],
          name: toJson['name'],
        );

        return org;
      } else {
        return null;
      }
    } catch (err) {
      return null;
    }
  }
  return null;
}
