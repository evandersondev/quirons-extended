import 'package:flutter/material.dart';

Future<DateTime?> selectBirthdate(BuildContext context) async => showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1930),
      lastDate: DateTime(2100),
    );
