import 'package:get/get.dart';
import 'package:quirons_user/app/graphql/gen/api.graphql.dart';
import 'package:graphql/src/exceptions/exceptions_next.dart';
import 'package:dartz/dartz.dart';
import 'package:quirons_user/app/interfaces/exam_repository_interface.dart';

import '../services/gql_service.dart';

class ExamRepository implements IExamRepository {
  final client = Get.find<GQLService>();

  @override
  Future<
      Either<OperationException,
          List<EmployeeConsultations$Query$EmployeeConsultations>>> list(
      BaseInput input) async {
    final query = EmployeeConsultationsQuery(
        variables: EmployeeConsultationsArguments(
      employee: input,
    ));

    final response = await client.query(query);

    if (response.exception == null) {
      return Right(response.data!.employeeConsultations);
    } else {
      return Left(response.exception!);
    }
  }
}
