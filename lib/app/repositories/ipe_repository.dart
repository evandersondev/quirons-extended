import 'package:dartz/dartz.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:quirons_user/app/interfaces/ipe_repository_interface.dart';
import '../graphql/gen/api.graphql.dart';
import '../services/auth_service.dart';
import '../services/gql_service.dart';

class IpeRepository implements IIpeRepository {
  final client = Get.find<GQLService>();
  final auth = AuthService();

  @override
  Future<Either<OperationException, List<EmployeeIpes$Query$EmployeeIpes>>>
      list(BaseInput input) async {
    final query =
        EmployeeIpesQuery(variables: EmployeeIpesArguments(employee: input));

    final response = await client.query(query);
    if (response.exception == null) {
      return Right(response.data?.employeeIpes ?? []);
    } else {
      return Left(response.exception!);
    }
  }
}
