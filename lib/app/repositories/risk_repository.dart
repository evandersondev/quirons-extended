import 'package:get/get.dart';
import 'package:quirons_user/app/graphql/gen/api.graphql.dart';
import 'package:graphql/src/exceptions/exceptions_next.dart';
import 'package:dartz/dartz.dart';
import 'package:quirons_user/app/interfaces/risk_repository_interface.dart';

import '../services/gql_service.dart';

class RiskRepository implements IRiskRepository {
  final client = Get.find<GQLService>();

  @override
  Future<Either<OperationException, List<EmployeeRisks$Query$EmployeeRisks>>>
      list(BaseInput input) async {
    final query = EmployeeRisksQuery(
        variables: EmployeeRisksArguments(
      employee: input,
    ));

    final response = await client.query(query);

    if (response.exception == null) {
      return Right(response.data!.employeeRisks);
    } else {
      return Left(response.exception!);
    }
  }
}
