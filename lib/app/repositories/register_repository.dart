import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../services/gql_service.dart';
import '../graphql/gen/api.graphql.dart';
import '../interfaces/register_repository_interface.dart';

class RegisterRepository implements IRegisterRepository {
  final client = Get.find<GQLService>();

  @override
  Future<Either<OperationException, GeneratePassword$Mutation>> register(
    BuildContext context, {
    required GeneratePasswordInput input,
  }) async {
    final mutation = GeneratePasswordMutation(
      variables: GeneratePasswordArguments(generate: input),
    );

    final response = await client.mutation(mutation);

    if (response.exception == null) {
      return Right(response.data!);
    } else {
      return Left(response.exception!);
    }
  }
}
