import 'package:get/get.dart';
import 'package:graphql/src/exceptions/exceptions_next.dart';
import 'package:dartz/dartz.dart';
import '../graphql/gen/api.graphql.dart';
import '../interfaces/cipa_repository_interface.dart';
import '../services/gql_service.dart';

class CipaRepository implements ICipaRepository {
  final client = Get.find<GQLService>();

  @override
  Future<
      Either<OperationException,
          EmployeeCandidate$Mutation$EmployeeCandidate>> candidate(
      EmployeeCandidateInput input) async {
    final mutation = EmployeeCandidateMutation(
        variables: EmployeeCandidateArguments(
            candidate: EmployeeCandidateInput(
      individualRegistration: input.individualRegistration,
      mandateId: input.mandateId,
      organizationId: input.organizationId,
      password: input.password,
    )));

    final response = await client.query(mutation);

    if (response.exception == null) {
      return Right(response.data!.employeeCandidate);
    } else {
      return Left(response.exception!);
    }
  }

  @override
  Future<
      Either<OperationException,
          EmployeeHasCandidate$Query$EmployeeHasCandidate>> hasCandidate(
      EmployeeCandidateInput input) async {
    final query = EmployeeHasCandidateQuery(
      variables: EmployeeHasCandidateArguments(
        employee: EmployeeHasCandidateInput(
          individualRegistration: input.individualRegistration,
          mandateId: input.mandateId,
          organizationId: input.organizationId,
          password: input.password,
        ),
      ),
    );

    final response = await client.query(query);

    if (response.exception == null) {
      return Right(response.data!.employeeHasCandidate!);
    } else {
      return Left(response.exception!);
    }
  }

  @override
  Future<Either<OperationException, EmployeeHasVote$Query$EmployeeHasVote?>>
      hasVote(EmployeeHasVoteInput input) async {
    final query = EmployeeHasVoteQuery(
      variables: EmployeeHasVoteArguments(
        employee: EmployeeHasVoteInput(
            organizationId: input.organizationId,
            individualRegistration: input.individualRegistration,
            password: input.password,
            mandateId: input.mandateId),
      ),
    );

    final response = await client.query(query);

    if (response.exception == null) {
      return Right(response.data!.employeeHasVote);
    } else {
      return Left(response.exception!);
    }
  }

  @override
  Future<
      Either<OperationException,
          List<EmployeeMandates$Query$EmployeeMandates>>> mandates(
      EmployeeMandateInput input) async {
    final query = EmployeeMandatesQuery(
        variables: EmployeeMandatesArguments(employee: input));

    final response = await client.query(query);

    if (response.exception == null) {
      return Right(response.data!.employeeMandates);
    } else {
      return Left(response.exception!);
    }
  }

  @override
  Future<Either<OperationException, EmployeeVote$Mutation$EmployeeVote>> vote(
      EmployeeVoteInput input) async {
    final mutation = EmployeeVoteMutation(
      variables: EmployeeVoteArguments(vote: input),
    );

    final response = await client.query(mutation);

    if (response.exception == null) {
      return Right(response.data!.employeeVote);
    } else {
      return Left(response.exception!);
    }
  }

  @override
  Future<Either<OperationException, List<EmployeeIpes$Query$EmployeeIpes>>>
      ipes(BaseInput input) async {
    final query =
        EmployeeIpesQuery(variables: EmployeeIpesArguments(employee: input));

    final response = await client.query(query);

    if (response.exception == null) {
      return Right(response.data!.employeeIpes);
    } else {
      return Left(response.exception!);
    }
  }

  @override
  Future<
          Either<OperationException,
              List<EmployeeConsultations$Query$EmployeeConsultations>>>
      consultation(BaseInput input) async {
    final query = EmployeeConsultationsQuery(
        variables: EmployeeConsultationsArguments(
      employee: input,
    ));

    final response = await client.query(query);

    if (response.exception == null) {
      return Right(response.data!.employeeConsultations);
    } else {
      return Left(response.exception!);
    }
  }
}
