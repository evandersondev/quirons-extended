import 'package:get/get.dart';
import 'package:quirons_user/app/graphql/gen/api.graphql.dart';
import 'package:graphql/src/exceptions/exceptions_next.dart';
import 'package:dartz/dartz.dart';

import '../interfaces/profile_repository_interface.dart';
import '../services/gql_service.dart';

class ProfileRepository implements IProfileRepository {
  final client = Get.find<GQLService>();

  @override
  Future<Either<OperationException, EmployeeInfo$Query$EmployeeInfo>> get(
      BaseInput input) async {
    final query = EmployeeInfoQuery(
      variables: EmployeeInfoArguments(employee: input),
    );

    final response = await client.query(query);

    if (response.exception == null) {
      return Right(response.data!.employeeInfo!);
    } else {
      return Left(response.exception!);
    }
  }
}
