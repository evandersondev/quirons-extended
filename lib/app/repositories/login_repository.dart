import 'package:dartz/dartz.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../graphql/gen/api.graphql.dart';
import '../interfaces/login_repository_interface.dart';
import '../services/auth_service.dart';
import '../services/gql_service.dart';

class LoginRepository implements ILoginRepository {
  final client = Get.find<GQLService>();
  final auth = AuthService();

  @override
  Future<Either<OperationException, EmployeeLogin$Query>> login(
    BuildContext context, {
    required BaseInput input,
  }) async {
    final query = EmployeeLoginQuery(
      variables: EmployeeLoginArguments(generate: input),
    );

    final response = await client.query(query);

    if (response.exception == null) {
      return Right(response.data!);
    } else {
      return Left(response.exception!);
    }
  }
}
