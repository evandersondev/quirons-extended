import 'package:flutter/material.dart';

class TwoColumnsWidget extends StatelessWidget {
  const TwoColumnsWidget(
    this.context, {
    required this.left,
    required this.right,
  });

  final BuildContext context;
  final Widget left;
  final Widget right;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.45,
          child: left,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.45,
          child: right,
        )
      ],
    );
  }
}
