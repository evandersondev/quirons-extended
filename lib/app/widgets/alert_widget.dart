import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

enum AlertAnimateEnum { zoom, fade }

class AlertWidget {
  static Future<void> show(
    AlertAnimateEnum animate, {
    required BuildContext context,
    required Widget title,
    required List<Widget> body,
    required List<Widget> actions,
  }) {
    final alert = AlertDialog(
      title: title,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      content: SingleChildScrollView(
        child: ListBody(children: body),
      ),
      actions: actions,
    );

    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        if (animate == AlertAnimateEnum.zoom) {
          return ZoomIn(
            duration: Duration(milliseconds: 200),
            child: alert,
          );
        } else {
          return FadeInUpBig(
            duration: Duration(milliseconds: 200),
            child: alert,
          );
        }
      },
    );
  }
}
