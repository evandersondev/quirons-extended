import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../services/error_service.dart';
import '../utils/adaptive_dialog.dart';
import '../utils/is_debug.dart';

/// Handle or ignores a operation exception
/// based on [globalHandledCodes] of [ErrorService]
/// if it's handled, it will show an alert dialog
/// else it does nothing
void handleOrIgnoreError(OperationException exception) {
  final errorService = ErrorService();

  final error = errorService.parse(exception).first;

  if (isDebug()) {
    print(exception.toString());
  }

  if (!errorService.isGlobalHandled(error)) {
    final message = errorService.extractMessage(error);

    unawaited(
      Get.dialog(
        AdaptiveDialog.alert(
          title: const Text('Ops!'),
          content: Text(message as String),
        ),
      ),
    );
  }
}

/// Default error handler for graphQL errors.
/// this mixin offers the default error handling
/// for operation exceptions.
mixin DefaultErrorHandler {
  @Deprecated('deprecated use directly handleOrIgnoreError')
  void handleOrIgnore(OperationException exception) =>
      handleOrIgnoreError(exception);
}
