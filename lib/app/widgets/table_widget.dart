import 'package:flutter/material.dart';

import '../constants/app_metrics.dart';

class TableWidget extends StatelessWidget {
  const TableWidget({
    this.title,
    required this.header,
    required this.body,
  });

  final String? title;
  final List<String> header;
  final List<List<String>?> body;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (title != null)
            Padding(
              padding: const EdgeInsets.all(paddingSize),
              child: Text(
                title!,
                style: const TextStyle(fontSize: 16),
              ),
            ),
          SingleChildScrollView(
            padding: const EdgeInsets.all(paddingSize),
            scrollDirection: Axis.horizontal,
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: List.generate(header.length, (index) {
                  return SizedBox(
                    width: header.length > 1
                        ? MediaQuery.of(context).size.width * 0.55
                        : MediaQuery.of(context).size.width -
                            (paddingSize * 4.8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(vertical: 8),
                          child: Text(
                            header[index],
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        ...body.map((e) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Divider(thickness: 2, color: Colors.grey[100]),
                              Container(
                                height: 46,
                                padding:
                                    const EdgeInsets.symmetric(vertical: 4),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(e![index]),
                                  ],
                                ),
                              ),
                            ],
                          );
                        }).toList(),
                      ],
                    ),
                  );
                })),
          ),
        ],
      ),
    );
  }
}
