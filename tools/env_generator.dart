import 'dart:convert';
import 'dart:io';

// ignore: avoid_positional_boolean_parameters
void requireCondition(bool condition, String message) {
  if (!condition) {
    throw Exception(message);
  }
}

Future<void> main() async {
  try {
    final quironsUrl = Platform.environment['QUIRONS_URL'];

    // QUIRONS URL
    requireCondition(quironsUrl != null, r'$KEEPFY_URL is required');
    requireCondition(quironsUrl != 'null', r'$KEEPFY_URL is required');
    requireCondition(
        // ignore: lines_longer_than_80_chars
        quironsUrl != null && quironsUrl.isNotEmpty,
        r'$KEEPFY_URL is must not be empty or blank');

    const filename = 'lib/generated_env.dart';
    final contents =
        '// ignore_for_file: public_member_api_docs, prefer_single_quotes\n${'const quironsUrl = ${json.encode(quironsUrl)};\n'}';

    print('Generating $filename');
    await File(filename).writeAsString(contents);
    print('Generated $filename');
  } on Exception catch (exception) {
    print(exception);
    exit(1);
  }
}
